﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIShoppinAndItemCounter : MonoBehaviour {

    /// <summary>
    /// UI
    /// </summary>
    public Button _btnBuy;
    public Button _btnUse;

    public GameObject _containerOneButton;
    public GameObject _containerTwoButton;
    public GameObject _containerItemUsingStatus;

    public Image _imgCoinIcon;

    public Text _txtLeftTime;
    public Text _txtDuration;
    public Text _txtValue;
    public Text _txtNum;

    public Text[] _aryTxtPriceDiamond;
    public Text _txtPriceCoin;
    public Text _txtPriceDiamond;

    public Image _imgAvatar;

    public Image _imgProgressBarFill;

    // end UI

    public float m_fValue = 0;
    public double m_dCoinPrice = 0;
    public int m_nDiamondPrice = 0;
    public int m_nDuration = 0;


    public int m_nItemType = 0; // 0 - 用法币买钻石  1 - 收益强化道具  2 - 技能点
    public int m_nItemSubType = 0;
    public string m_szFuncDest = "";

    bool m_bItemInBag = false;

    int m_nNum = 0;
    public int m_nItemId = 0;

    System.DateTime m_dateStartTime;
    bool m_bWorking = false;

    // Use this for initialization
    void Start () {

        UpdateUIInfo();


    }
	
	// Update is called once per frame
	void Update () {
     //  Loop();

    }

    public ShoppinMall.sShoppingMallItemConfig m_Config;
    public void Init(ShoppinMall.sShoppingMallItemConfig config )
    {
        m_Config = config;

        switch( (ShoppinMall.eItemType)config.itemConfig.nType )
        {
            case ShoppinMall.eItemType.diamond: // 商品是“钻石”
                {
                    _txtPriceDiamond.text = "$" + config.nPrice1;
                    _txtValue.text = config.szDesc;
                }
                break;

                // right here
            case ShoppinMall.eItemType.skill_point: // 技能点
                {
                    _txtPriceDiamond.text = config.nPrice1.ToString();
                    _txtValue.text = config.szDesc;

                    int nCurBuyTimes = ScienceTree.s_Instance.GetSkillPointBuyTimes( config.itemConfig.aryIntParams[0] );
                    double dTotalPriceOfThisGroup = 0d;
                    for (int i = 0; i < config.nNum; i++ )
                    {
                        double dRealTimePrice = ScienceTree.s_Instance.GetRealTimePriceByBuyTimes(config.nPrice2, nCurBuyTimes++, config.fRisePricePercent);
                        dTotalPriceOfThisGroup += dRealTimePrice;
                    }

                    _txtPriceCoin.text = CyberTreeMath.GetFormatMoney_New(dTotalPriceOfThisGroup);
                    m_dCoinPrice = dTotalPriceOfThisGroup;


                    int nSkillPointType = config.itemConfig.aryIntParams[0];
                    _imgAvatar.sprite = ItemSystem.s_Instance.GetItemIconById(config.nId);
                    _imgCoinIcon.sprite = ResourceManager.s_Instance.GetCoinSpriteByPlanetId(nSkillPointType);
                }
                break;

            case ShoppinMall.eItemType.coin_raise: // 金币收益加成道具
                {
                    _txtPriceDiamond.text = config.nPrice1.ToString();
                    _txtValue.text = config.szDesc;
                    _txtDuration.text = CyberTreeMath.FormatTime( config.itemConfig.nDuration );
                }
                break;

            case ShoppinMall.eItemType.automobile_accelerate: // 载具速度加成道具
                {
                    _txtPriceDiamond.text = config.nPrice1.ToString();
                    _txtValue.text = config.itemConfig.szName;
                    _txtDuration.text = CyberTreeMath.FormatTime(config.itemConfig.nDuration);
                }
                break;

            case ShoppinMall.eItemType.fast_forward: // 金币瞬时收益
                {
                    _txtPriceDiamond.text = config.nPrice1.ToString();
                    _txtValue.text = config.itemConfig.szName;
                    _txtDuration.text = "";
                }
                break;

        } // end switch

        if (_txtNum)
        {
            _txtNum.text = "";
        }
    }

    ShoppinMall.eItemType m_eItemType = ShoppinMall.eItemType.diamond;
    public ShoppinMall.eItemType GetItemType()
    {
        return m_eItemType;
    }

    public void SetItemType( ShoppinMall.eItemType item_type  )
    {
        m_eItemType = item_type;
    }

    public void OnClick_LegalMoneyBuy_0()
    {
        AccountSystem.s_Instance.SetGreenCash(AccountSystem.s_Instance.GetGreenCash() + m_Config.nNum);
        //AudioManager.s_Instance.PlaySE(AudioManager.eSE.e_buy_plane);

        AudioManager.s_Instance.PlaySE_New(AudioManager.eSe_New.HuoDeMeiChao);
    }

    public void OnClick_LegalMoneyBuy_1()
    {
        AccountSystem.s_Instance.SetGreenCash(AccountSystem.s_Instance.GetGreenCash() + 400);
        AudioManager.s_Instance.PlaySE(AudioManager.eSE.e_buy_plane);
    }

    public void OnClick_LegalMoneyBuy_2()
    {
        AccountSystem.s_Instance.SetGreenCash(AccountSystem.s_Instance.GetGreenCash() + 300);
        AudioManager.s_Instance.PlaySE(AudioManager.eSE.e_buy_plane);
    }

    public void OnClick_CoinBuy() // 目前走这个流程的只有技能点购买xw
    {
        if (m_Config.itemConfig.nType != (int)ShoppinMall.eItemType.skill_point )
        {
            return;
        }

        int nSkillPointType = m_Config.itemConfig.aryIntParams[0];

        if ( AccountSystem.s_Instance.GetCoin(nSkillPointType) < m_Config.nPrice2 )
        {
            UIMsgBox.s_Instance.ShowMsg( "金币不足" );
            return;
        }

        // right here
        double dCurCoin = AccountSystem.s_Instance.GetCoin(nSkillPointType);
        dCurCoin -= m_dCoinPrice;
        AccountSystem.s_Instance.SetCoin(nSkillPointType, dCurCoin);

        /*
        int nCurPoint = ScienceTree.s_Instance.GetSkillPoint((ScienceTree.eBranchType)nSkillPointType);
        ScienceTree.s_Instance.SetSkillPoint((ScienceTree.eBranchType)nSkillPointType, nCurPoint + 1);
*/
        ShoppinMall.s_Instance.OnBuySkillPointSucceeded(m_Config);

        AudioManager.s_Instance.PlaySE_New(AudioManager.eSe_New.GouMaiJiNengDian);
      //  AudioManager.s_Instance.PlaySE(AudioManager.eSE.e_buy_plane);
    }

    public void OnClick_DiamondBuy()
    {
        AudioManager.s_Instance.PlaySE_New(AudioManager.eSe_New.Default_Click_Button);

        int nCurDiamond = (int)AccountSystem.s_Instance.GetGreenCash();
        if (nCurDiamond < m_Config.nPrice1)
        {
            UIMsgBox.s_Instance.ShowMsg( "钻石不足 ");
            return;
        }

        ShoppinMall.s_Instance.ShowConfirmBuyPanel( this, 0 );
    }

    public void SetItemInBag( bool bItemInBag )
    {
        m_bItemInBag = bItemInBag;
        if (m_bItemInBag)
        {
            _btnBuy.gameObject.SetActive( false );
            _btnUse.gameObject.SetActive(true);
        }
    }

    public void OnClick_UseItem()
    {
        AudioManager.s_Instance.PlaySE_New(AudioManager.eSe_New.Default_Click_Button);

        ItemSystem.s_Instance.UseItem( this );

       // _btnUse.gameObject.SetActive( false );
    }

    public void CopyData( int nItemId  )
    {
        ItemSystem.sItemConfig item_config = ItemSystem.s_Instance.GetItemConfigById(nItemId);

        m_nDuration = item_config.nDuration;
        m_nItemType = item_config.nType;
        m_fValue = item_config.aryFloatParams[0];
        m_nItemId = item_config.nId;
        _txtValue.text = item_config.szName;
        _txtDuration.text = CyberTreeMath.FormatTime(m_nDuration);
        if ((ShoppinMall.eItemType)item_config.nType == ShoppinMall.eItemType.fast_forward)
        {
            _txtDuration.text = "";
        }
    //    UpdateUIInfo();
    }

    void UpdateUIInfo()
    {
        return; // 这个流程废弃

        if ( m_nItemType == 0 )
        {
            return;
        
        }

        _txtPriceCoin.text = m_dCoinPrice.ToString("f0");
        for (int i = 0; i < 2; i++)
        {
            _aryTxtPriceDiamond[i].text = m_nDiamondPrice.ToString();
        }

        if (m_nItemType == 2)
        {
            return;
        }
        _txtValue.text = "x" + (1f + m_fValue) + "倍";
        _txtDuration.text = CyberTreeMath.FormatTime(m_nDuration, 1);
 



    }

    public void Begin()
    {
        m_dateStartTime = Main.GetSystemTime();
        m_bWorking = true;

        _btnUse.gameObject.SetActive(false);

        _containerTwoButton.SetActive( false );
        _containerItemUsingStatus.SetActive( true );
    //    _containerOneButton.SetActive( false );
    //    _containerTwoButton.SetActive(false);
    }

    public void SetStartTime( System.DateTime dtStartTime )
    {
        m_dateStartTime = dtStartTime;
    }

    public int GetRealDuration()
    {
        return m_nDuration;
    }

    public void SetRealDuration( int nRealDuration )
    {
        m_nDuration = nRealDuration;
    }

    public System.DateTime GetStartTime()
    {
        return m_dateStartTime;
    }

    float m_fTimeElapse = 0;
    public void Loop()
    {
        if ( !m_bWorking)
        {
            return;
        }

      

        double fElapse = (Main.GetSystemTime() - m_dateStartTime).TotalSeconds;
//        Debug.Log(m_dateStartTime + " _ " + fElapse + "_" + m_nDuration);
        int fTimeLeft = m_nDuration - (int)fElapse;
        _imgProgressBarFill.fillAmount = (float)fElapse / (float)m_nDuration;
        _txtLeftTime.text = CyberTreeMath.FormatTime(fTimeLeft);
        if (fTimeLeft <= 0)
        {
            End();
        }


    }

    public void AddTime( int fTime )
    {
        m_nDuration += fTime;
    }

    public void End()
    {
        // ResourceManager.s_Instance.DeleteShoppinAndItemCounter( this );
        switch((ShoppinMall.eItemType)this.m_nItemType )
        {
            case ShoppinMall.eItemType.automobile_accelerate:
                {
                    Main.s_Instance. StopAccelerateAll( 1 );
                }
                break;
        } // end switch


        ItemSystem.s_Instance.RemoveFromUsingList(this);
        ItemSystem.s_Instance.RemoveItem( this );  
    }

    public void SetNum( int nNum )
    {
        m_nNum = nNum;
        _txtNum.text = m_nNum.ToString();
        if (m_nNum <= 0)
        {
            ItemSystem.s_Instance.RemoveItem(this);
        }
    }

    public int GetNum()
    {
        return m_nNum;
    }

} // end class
