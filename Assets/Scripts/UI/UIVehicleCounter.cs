﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIVehicleCounter : MonoBehaviour {

    static Color colorTemp = new Color();


    /// <summary>
    /// UI
    /// </summary>
    public GameObject _containerPriceOff;
    public Text _txtPriceOff;

    public Text _txtQuestionMark0;
    public Text _txtQuestionMark1;
    public GameObject _containerUnlockLevelAvatar;
    public Image _imgUnlockLevelAvatar;
    public Image _imgLock;


    public Text _txtTitleSpeed;
    public Text _txtTitleGain;

    public Text _txtLevel;
    public Image _imgAvatar;
    public Text _txtSpeed;
    public Text _txtGain;
    public Text _txtPrice;

    public Text _txtInitialPrice;
    public Text _txtSkillDiscount;
    public Text _txtScienceDiscount;

    public Text _txtName;

    public Text _txtUnLockLevel;

    public Image _imgProgressBar_Gain;
    public Image _imgProgressBar_Speed;

    public Image _imgBtnBuy;
    public Image _imgCoinType;


    public GameObject _containerGainAndSpeed;
    public GameObject _containerDiscount;
    public GameObject _containerPrice;

    public Button _btnBuy;

    public GameObject _containerResearchLock;

    // end UI

    /// <summary>
    /// Logic Data
    /// </summary>
    public int m_nVehicleLevel;
    public double m_nPrice;
    public int m_nDiamondPrice;
    public bool m_bWatchAdFree = false;
    public DataManager.eMoneyType m_eMoneyType;
    private Sprite m_sprJianYingTemp;

    public DataManager.sAutomobileConfig m_Config;

    bool m_bUnlocked = false;

    ResearchCounter m_BoundResearchCounter = null;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnClickBuy()
    {
        bool bRet = false;

        if (m_bWatchAdFree)
        {


        }
        else
        {
            if (m_eMoneyType == DataManager.eMoneyType.diamond) // 钻石
            {
                if (AccountSystem.s_Instance.GetGreenCash() < m_nDiamondPrice)
                {
                    UIMsgBox.s_Instance.ShowMsg("钻石不够");
                    return;
                }
            }
            else // 金币
            {
                if (MapManager.s_Instance.GetCurPlanet().GetCoin() < m_nPrice)
                {
                    UIMsgBox.s_Instance.ShowMsg("金币不够");
                    return;
                }
            }
        }

        Plane plane = null;
        bRet = Main.s_Instance.BuyOneVehicle( m_nVehicleLevel, ref plane);
        if (bRet)
        {
   //         UIMsgBox.s_Instance.ShowMsg("购买成功");

            if (m_bWatchAdFree)
            {
                TanGeChe.s_Instance.m_dtLastWatchAdsTime = Main.GetSystemTime();
                AdsManager.s_Instance.PlayAds();

            }
            else
            {
                if (m_eMoneyType == DataManager.eMoneyType.diamond) // 钻石
                {
                    AccountSystem.s_Instance.SetGreenCash(AccountSystem.s_Instance.GetGreenCash() - m_nDiamondPrice);
                }
                else
                {
                    MapManager.s_Instance.GetCurPlanet().SetCoin(MapManager.s_Instance.GetCurPlanet().GetCoin() - m_nPrice);
                }
            }

            //MapManager.s_Instance.GetCurDistrict().AddBuyTimes(m_nVehicleLevel);
            /*
            int nCurTimes = MapManager.s_Instance.GetCurDistrict().GetVehicleBuyTimeById( m_Config.nLevel);
            nCurTimes++;
            MapManager.s_Instance.GetCurDistrict().SetVehicleBuyTimeById(m_Config.nLevel, nCurTimes);
            */

          //  UpdatePrice();

            TanGeChe.s_Instance.UpdateCarMallInfo();
        }

    }
    /*
    public void UpdatePrice()
    {
        int nPrice = DataManager.s_Instance.GetVehiclePrice(MapManager.s_Instance.GetCurPlanet().GetId(), MapManager.s_Instance.GetCurDistrict().GetId(), m_nVehicleLevel);
        _txtPrice.text = nPrice.ToString();;
        m_nPrice = nPrice;


    }
    */
    public bool GetUnlocked()
    {
        return m_bUnlocked;
    }

    public void SetUnlocked( bool bUnloceked )
    {
        m_bUnlocked = bUnloceked;

        if (!bUnloceked)
        {
            colorTemp = Color.black;

            _txtTitleGain.color = TanGeChe.s_Instance.m_colorLocked;
            _txtTitleGain.gameObject.GetComponent<Outline>().enabled = false;
            _txtTitleSpeed.gameObject.GetComponent<Outline>().enabled = false;
            _txtTitleSpeed.color = TanGeChe.s_Instance.m_colorLocked;
            _imgAvatar.color = TanGeChe.s_Instance.m_colorLocked;
        //   _imgAvatar.sprite = TanGeChe.s_Instance.m_sprJianYingTemp;
            _txtPrice.text = "???";
            _txtGain.text = "?";
            _txtSpeed.text = "?";
            _txtName.text = "???";
            _txtName.color = TanGeChe.s_Instance.m_colorLocked;
            _imgProgressBar_Gain.fillAmount = 0;
            _imgProgressBar_Speed.fillAmount = 0;

            _containerDiscount.SetActive(false);
            _containerGainAndSpeed.SetActive(false);
           // _containerPrice.SetActive(false);

            _imgBtnBuy.sprite = TanGeChe.s_Instance.m_sprLockedBuyButton;

            _btnBuy.enabled = false;

           // _txtUnLockLevel.gameObject.SetActive( true );
            _imgCoinType.gameObject.SetActive(false);
        }
        else
        {
            _imgProgressBar_Gain.fillAmount = 0.3f;
            _imgProgressBar_Speed.fillAmount = 0.7f;

            _imgAvatar.color = Color.white;
            _txtName.color = TanGeChe.s_Instance.m_colorUnlocked;
            _containerDiscount.SetActive(true);
            _containerGainAndSpeed.SetActive(true);
            _containerPrice.SetActive(true);

            _imgBtnBuy.sprite = TanGeChe.s_Instance.m_spUnlockedBuyButton;
            _btnBuy.enabled = true;

            _txtUnLockLevel.gameObject.SetActive(false);
            _imgCoinType.gameObject.SetActive(true);

            _txtTitleGain.color = Color.white;
            _txtTitleSpeed.color = Color.white;
            _txtTitleGain.gameObject.GetComponent<Outline>().enabled = true;
            _txtTitleSpeed.gameObject.GetComponent<Outline>().enabled = true;
        }
    }

    public void SetBoundResearchCounter( ResearchCounter counter )
    {
        m_BoundResearchCounter = counter;

      // counter.transform.SetParent( _containerResearchLock.transform );
    }

    public ResearchCounter GetBoundResearchCounter()
    {
        return m_BoundResearchCounter;
    }

} // end class
