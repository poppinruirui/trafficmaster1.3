﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class AdsManager : MonoBehaviour {

    public static AdsManager s_Instance = null;

    public float m_fTotalTime = 10000;
    float m_fCurTime = 0;

    public GameObject _panelAds;

    /// <summary>
    /// UI
    /// </summary>
    public GameObject _panelPreAds;
    public Text _txtAdsLeftTime;
    public Text _txtTitleTiSheng;
    public Text _txtTitleTiShengJiaoBiao;

    public Text _txtTitle;
    public Text _txtDesc;
    public Text _txtLeftTime;

    public Image _imgProgressBar;

    public float m_fBaseRaise = 2f;     // 广告的金币加成百分比
    public float m_fRaiseDuration = 0f; // 广告加成效果的持续时间
    public float m_fAdsPlayTime = 0f; // 一则广告的平均播放时长

    // 广告的实际加成、实际持续时间、实际总时间还要受天赋点的影响，不一定等于初始配置的数值。
    float m_fRealCoinPromote = 0f;
    float m_fRealDuraton = 0f;
    float m_fRealTotalTime = 0f;

    eAdsType m_eAdsType = eAdsType.common;

    


    public enum eAdsType
    {
        common,
        bat_collect_offline,
        bat_ads_raise,
        collect_offline_profit_watching_ads,
        collect_offline_profit_using_diamond,
    };

    public void SetAdsType(eAdsType eType)
    {
        m_eAdsType = eType;
    }

    private void Awake()
    {
        s_Instance = this;
    }


    // Use this for initialization
    void Start () {
        string szConfigFileName_Ads = DataManager.url + "ads.csv";
        StartCoroutine(LoadConfig_Ads(szConfigFileName_Ads));
       // LoadConfigOffline_Ads(szConfigFileName_Ads);
    }
	
	// Update is called once per frame
	void Update () {
        MainLoop();
	}

    public void LoadConfigOffline_Ads(string szFileName)
    {
        StreamReader sr = new StreamReader(szFileName);
        string szAll = sr.ReadToEnd();         string[] aryLines = szAll.Split('\n');

        string[] aryGeneralParams = aryLines[1].Split(',');
        int nIndex = 0;
        m_fAdsPlayTime = float.Parse(aryGeneralParams[nIndex++]);
        m_fRaiseDuration = float.Parse(aryGeneralParams[nIndex++]);
        m_fBaseRaise = float.Parse(aryGeneralParams[nIndex++]);
        m_fTotalTime = float.Parse(aryGeneralParams[nIndex++]);

        m_fAdsPlayTime = m_fRaiseDuration;

        m_bAdsConfigLoaded = true;
        DataManager.s_Instance.TryLoadMyDataCurTrackPlanesData();

    } // end LoadConfigOffline_Ads


    public bool m_bAdsConfigLoaded = false;
    IEnumerator LoadConfig_Ads(string szFileName)
    {
        WWW www = new WWW(szFileName);
        yield return www; // 等待下载

        string[] aryLines = www.text.Split('\n');

        string[] aryGeneralParams = aryLines[1].Split( ',' );
        int nIndex = 0;
        m_fAdsPlayTime = float.Parse(aryGeneralParams[nIndex++]);
        m_fRaiseDuration = float.Parse(aryGeneralParams[nIndex++]);
        m_fBaseRaise = float.Parse(aryGeneralParams[nIndex++]);
        m_fTotalTime = float.Parse(aryGeneralParams[nIndex++]);

        m_fAdsPlayTime = m_fRaiseDuration;

        m_bAdsConfigLoaded = true;
        DataManager.s_Instance.TryLoadMyDataCurTrackPlanesData();
    } // end LoadConfig_Ads




    public  void OnClick_CloseAds()
    {
        _panelAds.SetActive(false);

        switch (m_eAdsType)
        {
            case eAdsType.common:
                {
                    //MapManager.s_Instance.GetCurDistrict().GainAdsRaise();
                    Bat_Ads_Raise();
                }
                break;

            case eAdsType.bat_collect_offline:
                {
                    CollectAllDistrictsOfflineGainOfThisPlanet();
                }
                break;
            case eAdsType.bat_ads_raise:
                {
                    Bat_Ads_Raise();
                }
                break;
            case eAdsType.collect_offline_profit_watching_ads:
                {
                    District district = MapManager.s_Instance.GetCurDistrict();
                    float fOfflineProfit = Main.s_Instance.m_fOfflineProfit * ( 1f + OfflineManager.s_Instance.GetRealWatchAdsPromotePercent() );
                    Debug.Log( "获得离线收益：" + fOfflineProfit);


                    Planet planet = MapManager.s_Instance.GetCurPlanet();

                    planet.SetCoin(planet.GetCoin() + (int)fOfflineProfit);
                    district.SetCurTotalOfflineGain(0);

                    Main.s_Instance.CollectOfflineProfitAnimation();
                }
                break;

            case eAdsType.collect_offline_profit_using_diamond:
                {
                    District district = MapManager.s_Instance.GetCurDistrict();
                    float fOfflineProfit = Main.s_Instance.m_fOfflineProfit * (1f + OfflineManager.s_Instance.GetRealUsingDiamondPromotePercent());
                    Debug.Log("获得离线收益：" + fOfflineProfit);


                    Planet planet = MapManager.s_Instance.GetCurPlanet();

                    planet.SetCoin(planet.GetCoin() + (int)fOfflineProfit);
                    district.SetCurTotalOfflineGain(0);

                    Main.s_Instance.CollectOfflineProfitAnimation();
                }
                break;

        } // end switch
    }

    public float GetAdsDuration()
    {
        float fRealDuration = 0;

        return m_fRealDuraton;
    }

    System.DateTime m_dtTotalTimeStartTime;
    public void Bat_Ads_Raise()
    {
        Planet planet = MapManager.s_Instance.GetPlanetById(MapManager.s_Instance.m_nCurShowPlanetDetialIdOnUI);
        District[] aryDistricts = planet.GetDistrictsList();
        for (int i = 0; i < aryDistricts.Length; i++)
        {
            District district = aryDistricts[i];
            if (district.GetStatus() != MapManager.eDistrictStatus.unlocked)
            {
                continue;
            }
            district.GainAdsRaise();
        }

        if (m_fCurTime <= 0)
        {
            m_dtTotalTimeStartTime = Main.GetSystemTime();
        }
        m_fCurTime += m_fAdsPlayTime;
        _imgProgressBar.fillAmount = m_fCurTime / m_fTotalTime;

        SaveData();
    }

    public void SaveData()
    {
        string szAdsPromoteInfo = "";
        string szKey = "AdsTotalTime";
        string szData = AdsManager.s_Instance.GetTotalTime() + "," + GetAdsRaise(ref szAdsPromoteInfo ) + "," + m_dtTotalTimeStartTime;
        DataManager.s_Instance.SaveMyData(szKey, szData);
    }

    // right here
    void CollectAllDistrictsOfflineGainOfThisPlanet()
    {
        string szDetails = "";
        string szTitles = "";
        float fTotal = 0;
        Planet planet = MapManager.s_Instance.GetPlanetById(MapManager.s_Instance.m_nCurShowPlanetDetialIdOnUI);
        District[] aryDistricts = planet.GetDistrictsList();
        for (int i = 0; i < aryDistricts.Length; i++ )
        {
            District district = aryDistricts[i];


            DataManager.sTrackConfig track_config = DataManager.s_Instance.GetTrackConfigById(district.GetBoundPlanet().GetId(), district.GetId());
            szTitles += (track_config.szName + "\n");

            if (district.GetStatus() != MapManager.eDistrictStatus.unlocked)
            {
                szDetails += ("<Color=#C8C8C8>赛道未解锁</Color>\n");
                continue;
            }
            if (district == MapManager.s_Instance.GetCurDistrict())
            {
                szDetails += ("<Color=#44d4f1>当前赛道</Color>\n");
                continue;
            }
            float fSpanTime = 0;
            float fGainOfThisDistrict = district.CalculateOffLineProfit( ref fSpanTime);  
            fTotal += fGainOfThisDistrict;
            district.SetCurTotalOfflineGain(0);
            szDetails += ("<Color=#FEE834>" + CyberTreeMath.GetFormatMoney( fGainOfThisDistrict) + "</Color>" + "\n" );

           
        } // end for 

        MapManager.s_Instance._txtBatOfflineTitle.text = szTitles;

        float fDouble = fTotal * 2;
        MapManager.s_Instance._txtBatOfflineGain.text = CyberTreeMath.GetFormatMoney( fTotal) + " X 2 = " + CyberTreeMath.GetFormatMoney(fDouble);
        MapManager.s_Instance._txtBatOfflineDetail.text = szDetails;

        MapManager.s_Instance._subpanelBatCollectOffline.SetActive(true );

        planet.SetCoin( planet.GetCoin() + (int)fTotal);
    }

    public void OnClick_ClosebatCollectionPanel()
    {
        MapManager.s_Instance._subpanelBatCollectOffline.SetActive(false);

    }

    // right here
    public void SetAdsRaise( float fAdsRaise )
    {
        m_fRealCoinPromote = fAdsRaise;
    }

    // right here
    public float GetAdsRaise( ref string szAdsPromoteInfo)   
    {
        // return m_fBaseRaise * ( 1f + ScienceTree.s_Instance.GetAdsCoinRaise() );
       
        szAdsPromoteInfo = "";
        szAdsPromoteInfo += "广告总提升: X" + m_fRealCoinPromote.ToString("f2") + "倍";
        if (m_fPromotePercent > 0)
        {
            szAdsPromoteInfo += "(原始值：" + (m_fBaseRaise * 100 ).ToString("f0") + "%；天赋点加成" + (m_fPromotePercent * 100).ToString( "f0" )+ ")";
        }
        szAdsPromoteInfo += "\n";
        szAdsPromoteInfo += "--------------------------------\n";

        return m_fRealCoinPromote;
    }

    public void OnClick_OpenAds()
    {
        _panelPreAds.SetActive( true );


        UpdateRealData();

        AudioManager.s_Instance.PlaySE_New(AudioManager.eSe_New.GuangGaoAnNiu);
    }

    float m_fPromotePercent = 0;

    public void UpdateRealData()
    {
        m_fPromotePercent = ScienceTree.s_Instance.GetAdsPromotePercent();

        /*
        m_fRealCoinPromote = (1f + m_fBaseRaise);
        if (m_fPromotePercent > 0)
        {
            m_fRealCoinPromote = (1f + m_fPromotePercent) * m_fRealCoinPromote;
        }*/
        m_fRealCoinPromote = 1f + m_fBaseRaise * (1 + m_fPromotePercent);

        float fDurationPromotePercent = ScienceTree.s_Instance.GetAdsDurationPromotePercent();
        m_fRealDuraton = m_fRaiseDuration;
        if (fDurationPromotePercent > 0)
        {
            m_fRealDuraton = (1f + fDurationPromotePercent) * m_fRealDuraton;
        }

        _txtTitle.text = "广告提升" ;//"X" + m_fRealCoinPromote.ToString("f2") + "倍广告收入";
        _txtDesc.text = CyberTreeMath.FormatTime( (int)m_fRealDuraton ) + " 内 <Color=#FCF34D>" + m_fRealCoinPromote.ToString("f2") + "</Color> 倍赛道收益";

        float fTotalTimePromotePercent = ScienceTree.s_Instance.GetAdsTotalTimePromotePercent();
        m_fRealTotalTime = m_fTotalTime;
        if (fTotalTimePromotePercent > 0)
        {
            m_fRealTotalTime = (1f + fTotalTimePromotePercent) * m_fRealTotalTime;
        }
        // m_fRealTotalTime
    }

    public void PlayAds()
    {
        Handheld.PlayFullScreenMovie("ads0.mp4", Color.black, FullScreenMovieControlMode.CancelOnInput);
    }

    public void OnClick_WatchAds()
    {
        if ( m_fTotalTime - m_fCurTime < m_fAdsPlayTime  )
        {
            UIMsgBox.s_Instance.ShowMsg( "广告剩余总时间不足" );
            return;
        }

        _panelAds.SetActive(true);
        SetAdsType(eAdsType.common);
        // _panelPreAds.SetActive(false);

        Handheld.PlayFullScreenMovie("ads0.mp4", Color.black, FullScreenMovieControlMode.CancelOnInput);
        OnClick_CloseAds();

        AudioManager.s_Instance.PlaySE_New(AudioManager.eSe_New.Default_Click_Button);
    }

    public void OnClick_ClosePreAds()
    {
        _panelPreAds.SetActive(false);

        AudioManager.s_Instance.PlaySE_New(AudioManager.eSe_New.GuanBiJieMianAnNiu);
    }

    public void OnClick_OpenAds_CollectAllDistrcitOfflineGainsOfThisPlanet()
    {
        _panelAds.SetActive(true);
        SetAdsType(eAdsType.bat_collect_offline);

        Handheld.PlayFullScreenMovie("ads0.mp4", Color.black, FullScreenMovieControlMode.CancelOnInput);
        OnClick_CloseAds();

    }

    public void OnClick_OpenAds_BatAdsRaise()
    {
        UpdateRealData();

        _panelAds.SetActive(true);
        SetAdsType(eAdsType.bat_ads_raise);

        Handheld.PlayFullScreenMovie("ads0.mp4", Color.black, FullScreenMovieControlMode.CancelOnInput);
        OnClick_CloseAds();
    }

    float m_fMainLoopTimeElapse = 0f;
    public void MainLoop()
    {
        m_fMainLoopTimeElapse += Time.deltaTime;
        if (m_fMainLoopTimeElapse < 1f)
        {
            return;
        }
        m_fMainLoopTimeElapse = 0;

        Planet[] aryPlanets = MapManager.s_Instance.GetPlanetList();
        if (aryPlanets == null)
        {
            return;
        }
        for (int i = 0; i < aryPlanets.Length; i++ )
        {
            Planet planet = aryPlanets[i];
            District[] aryDistricts = planet.GetDistrictsList();
            for (int j = 0; j < aryDistricts.Length; j++ )
            {
                District district = aryDistricts[j];
                district.AdsRaiseTimeLoop();
                if (district == MapManager.s_Instance.GetCurDistrict())
                {
                    float fLeftTime = district.GetAdsLeftTime();
                    ShowLeftTime(fLeftTime);
                    if (fLeftTime <= 0)
                    {
                       // _txtLeftTime.gameObject.SetActive( false );
                        _txtTitle.gameObject.SetActive( true );
                    }
                }
            } // end for j
        } // end for i

        UpdateTime();

    }

    // 总剩余时间 right here
    float m_fTotalTimeTimeElaspe = 0;
    void UpdateTime()
    {
        if (m_fCurTime <= 0 )
        {
            return;
        }

        float fTimeElapse = (float)(Main.GetSystemTime() - m_dtTotalTimeStartTime).TotalSeconds;
        float dTimeLeft = m_fCurTime - fTimeElapse;

        _txtLeftTime.text = CyberTreeMath.FormatTime((int)dTimeLeft) + "/" + CyberTreeMath.FormatTime((int)m_fRealTotalTime);
        if (dTimeLeft <= 0)
        {
            SaveData();
            m_fCurTime = 0;
        }
        /*
        m_fCurTime -= 1f;
        if (m_fCurTime <= 0)
        {
            m_fCurTime = 0;
        }
        _imgProgressBar.fillAmount = m_fCurTime / m_fRealTotalTime;
        _txtLeftTime.text = CyberTreeMath.FormatTime((int)m_fCurTime) + "/" + CyberTreeMath.FormatTime( (int)m_fRealTotalTime);

        */

        m_fTotalTimeTimeElaspe += 1f;
        if (m_fTotalTimeTimeElaspe < 10f)
        {
            return;
        }
        m_fTotalTimeTimeElaspe = 0;
        SaveData();
    }

    public void SetTotalTimeStartTime( System.DateTime dtTotalTimeStartTime)
    {
        m_dtTotalTimeStartTime = dtTotalTimeStartTime;
    }

    public void SetTotalTime( float nTotaltime )
    {
        m_fCurTime = nTotaltime;
    }

    public float GetTotalTime()
    {
        return m_fCurTime;
    }

    public void ShowLeftTime( float fLeftTime )
    {
        if (fLeftTime <= 0)
        {
            _txtAdsLeftTime.text = "";
            // _txtTitleTiSheng.gameObject.SetActive( true );
            SetTiShengVisible( true );
        }
        else
        {
            // _txtTitleTiSheng.gameObject.SetActive(false);
            SetTiShengVisible(false);
            _txtAdsLeftTime.text = CyberTreeMath.FormatTime((int) fLeftTime );
        }
    }


    public void SetTiShengVisible( bool bVisible )
    {
        _txtTitleTiSheng.gameObject.SetActive(bVisible);
        _txtTitleTiShengJiaoBiao.gameObject.SetActive(bVisible);
    }

} // end class
