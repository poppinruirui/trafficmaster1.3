﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxManager : MonoBehaviour {

    public static BoxManager s_Instance = null;

    static Vector3 vecTempPos = new Vector3();

    public float m_fGenerateBoxInterval = 15f;

    float m_fTimeElapse = 10f;

    float m_fDropStartPosY = 10f;
    float m_fDropSpeed = 0f;
    public float m_fDropTime = 1f;


    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if ( TanGeChe.s_Instance.m_bNoDrop )
        {
            return
                ;
        }

      GenerateLoop();
	}

    void GenerateLoop()
    {

        if (MapManager.s_Instance.GetCurDistrict() == null)
        {
            return;
        }

        // 掉落的时间间隔
        m_fTimeElapse += Time.deltaTime;
        int nDropInterval = MapManager.s_Instance.GetCurDistrict().GetDropInterval();
        if (nDropInterval <= 0)
        {
            return;
        }
        if (m_fTimeElapse < nDropInterval)
        {
            return;
        }
        m_fTimeElapse = 0;

        // 寻找一个空闲的泊位。如果没有了空闲泊位，也是不能掉落的
        Lot lot = Main.s_Instance.GetOneAvailableLotToGenerateBox();
        if ( lot == null )
        {
            return;
        }

        int nDropLevel = MapManager.s_Instance.GetCurDistrict().GetDropLevel();
        if (nDropLevel <= 0)
        {
            return;
        }


        Plane plane = ResourceManager.s_Instance.NewPlane();


        vecTempPos.x = 0f;
        vecTempPos.y = m_fDropStartPosY;
        vecTempPos.z = 0f;
        plane.transform.position = vecTempPos;


        plane.SetLevel(nDropLevel);
        lot.SetPlane(plane, false);

        vecTempPos = plane.GetPos();
        vecTempPos.x = 0;
        vecTempPos.z = -1;
        plane.SetPos(vecTempPos);

        float fDropDistance = vecTempPos.y;

        plane.ShowTreasureBox(fDropDistance);

        plane.SetPlaneStatus(Main.ePlaneStatus.treasure_box);

      
    }

   

}
