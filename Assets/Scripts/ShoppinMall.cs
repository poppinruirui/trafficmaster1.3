﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class ShoppinMall : MonoBehaviour {

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecScale = new Vector3();

    public static ShoppinMall s_Instance = null;

    public VerticalLayoutGroup _vlg;


    //// Prefab
    public GameObject m_preTypeContainer;
    public GameObject m_preItemCounter;
    public GameObject[] m_aryCounterPrefab;

    /// <summary>
    /// Data
    /// </summary>
    public int m_nStandardCounterNumPerRow = 3; // 每行多少个标准购物框
    public float m_fStandardCounterWidth = 0;   // 标准购物框的宽度
    public float m_fStandardCounterHeight = 0;   // 标准购物框的高度
    public float m_fStandardCounterHeight_Long = 0;   // 长购物框的高度
    public float m_fTypeContainerTitleHeight = 30f;

    public Vector2 m_vecTypeContainerPos = new Vector2();

    //// UI

    public GameObject m_goContainerAllType;


    public GameObject _panelShoppingMall;
    public GameObject _subpanelConfirmBuy;
    public GameObject _subpanelConfirmUse;

    public UIItem _CurProcessingBuyUiItem = null;
    public UIItemInBag _CurProcessingUsingUiItem = null;

    /// <summary>
    /// confirm buy
    /// </summary>
    public Text _txtConfirmBuy_FuncAndValue;
    public Text _txtConfirmBuy_Cost;
    public Text _txtConfirmBuy_ItemName;
    public Text _txtConfirmBuy_Value;
    public Image _imgConfirmBuy_Avatar;
    public Image _imgConfirmUse_Avatar;
    public sShoppingMallItemConfig m_ConfirmBuyShoppingConfig;
    double m_nConfirmBuy_Price = 0; // 买的价格
    int m_nConfirmBuy_Num = 0;   // 买的个数
    UIItem m_itemConfirmBuy = null;

    // 商城道具类型
    public enum eItemType
    {
        diamond,     // 钻石 
        coin,        // 金币
        skill_point, // 技能点
        coin_raise,   // 金币加成
        fast_forward,  // 瞬间提前获取收益 
        automobile_accelerate,   //  载具
        treassure_box, // 宝箱
        month_card,  // 月卡
    };

    // 商城货币类型
    public enum ePriceType
    {
        legal_tender, // 现实世界中的法币
        green_cash,  // 绿票(钻石)
        coin,        // 金币
    };

    // 货币的子类型
    public enum ePriceSubType
    {
        coin_0, // 0号星球的
        coin_1, // 1号星球的
        coin_2, // 2号星球的
    };

    public struct sShoppingMallItemConfig
    {
        public int nId;         // 物品id,对应于item.txt中的第一列id

        public ItemSystem.sItemConfig itemConfig; // 道具表中的配置

       
        public int nPriceType1; // 价格类型1
        public int nPriceSubType1; // 价格子类型1
        public double nPrice1;     // 价格1
        public int nNum1;       // 数量1

        public int nPriceType2; // 价格类型2
        public int nPriceSubType2; // 价格子类型2
        public double nPrice2;     // 价格2(金币价格)
        public int nNum2;       // 数量2
        public float fRisePricePercent; // 涨价百分比

        public int nNum; // 数量

        public int nOn;         // 是否上架 1 - 上架   0 - 下架

        public string szDesc;   // 描述

        public float fPriceRiseEachBuy; // 每次购买之后金币价格上涨
    };
    sShoppingMallItemConfig tempShoppinMallItemConfig;

    Dictionary<eItemType, List<sShoppingMallItemConfig>> m_dicShoppingMallItems = new Dictionary<eItemType, List<sShoppingMallItemConfig>>();


    Dictionary<int, int> m_dicItemBuyTimes = new Dictionary<int, int>();

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
        Init();

    }

    public const int MAX_ITEM_TYPE = 16;


 
	// Update is called once per frame
	void Update () {

    }


   
    public void LoadConfig()
    {
        string szConfigFileName_Shoppingmall = DataManager.url + "shoppingmall.csv";
        StartCoroutine(LoadConfig_Shoppingmall_New(szConfigFileName_Shoppingmall));
    //   LoadConfigOffline_Shoppingmall_New(szConfigFileName_Shoppingmall);

    }


    public List<sShoppingMallItemConfig> GetItemListByType( int nTypeId )
    {
        List<sShoppingMallItemConfig> lst = null;
        /*
        if ( !m_dicShoppingMallItems.TryGetValue(nTypeId, out lst) )
        {
            lst = new List<sShoppingMallItemConfig>();
            m_dicShoppingMallItems[nTypeId] = lst;
        }
        */
        return lst;
    }

    public string GetTypeNameById( int nTypeId )
    {
        string szTypeName = "error";

        switch ((ShoppinMall.eItemType)nTypeId)
        {
            case ShoppinMall.eItemType.diamond:
                {
                    szTypeName = "钻石";
                }
                break;
            case ShoppinMall.eItemType.coin:
                {
                    szTypeName = "金币";
                }
                break;
            case ShoppinMall.eItemType.skill_point:
                {
                    szTypeName = "天赋点";
                }
                break;
            case ShoppinMall.eItemType.coin_raise:
                {
                    szTypeName = "金币增收";
                }
                break;

            case ShoppinMall.eItemType.automobile_accelerate:
                {
                    szTypeName = "载具加速";
                }
                break;
            case ShoppinMall.eItemType.fast_forward:
                {
                    szTypeName = "瞬间收益";
                }
                break;
            case ShoppinMall.eItemType.treassure_box:
                {
                    szTypeName = "宝箱";
                }
                break;
        } // end switch

        return szTypeName;
    }

    public void LoadConfigOffline_Shoppingmall_New(string szFileName)
    {
        StreamReader sr = new StreamReader(szFileName);
        string szAll = sr.ReadToEnd();         string[] aryLines = szAll.Split('\n');

        for (int i = 1; i < aryLines.Length; i++)
        {
            int nPointer = 0;


            string[] aryParams = aryLines[i].Split(',');
            int nId = 0;
            if (!int.TryParse(aryParams[nPointer++], out nId))
            {
                continue;
            }

            sShoppingMallItemConfig config = new sShoppingMallItemConfig();

            config.nId = nId;
            config.itemConfig = ItemSystem.s_Instance.GetItemConfigById(nId);

            if (!double.TryParse(aryParams[nPointer++], out config.nPrice1))
            {

            }


            /*
            if (!double.TryParse(aryParams[7], out config.nPrice2))
            {

            }
            */
            string szPrice2 = aryParams[nPointer++];
            if (szPrice2 != "" && config.itemConfig.nType == (int)eItemType.skill_point)
            {
                string[] aryPrice2 = szPrice2.Split('_');
                config.nPrice2 = double.Parse(aryPrice2[0]);
                config.fRisePricePercent = float.Parse(aryPrice2[1]);
            }


            if (!int.TryParse(aryParams[nPointer++], out config.nNum))
            {
                config.nNum = 1;
            }



            if (!int.TryParse(aryParams[nPointer++], out config.nOn))
            {
                config.nOn = 0;
            }


            config.szDesc = aryParams[nPointer++];


            List<sShoppingMallItemConfig> lst = null;
            if (!m_dicShoppingMallItems.TryGetValue((eItemType)config.itemConfig.nType, out lst))
            {
                lst = new List<sShoppingMallItemConfig>();
            }
            lst.Add(config);

            m_dicShoppingMallItems[(eItemType)config.itemConfig.nType] = lst;

        } // end for i


    } // end LoadConfigOffline_Shoppingmall_New


    IEnumerator LoadConfig_Shoppingmall_New(string szFileName)
    {
        WWW www = new WWW(szFileName);
        yield return www; //  待下载

        string[] aryLines = www.text.Split('\n');
        for (int i = 1; i < aryLines.Length; i++)
        {
            int nPointer = 0;


            string[] aryParams = aryLines[i].Split(',');
            int nId = 0;
            if (!int.TryParse(aryParams[nPointer++], out nId))
            {
                continue;
            }

            sShoppingMallItemConfig config = new sShoppingMallItemConfig();

            config.nId = nId;
            config.itemConfig = ItemSystem.s_Instance.GetItemConfigById(nId);

            if (!double.TryParse(aryParams[nPointer++], out config.nPrice1))
            {

            }


            /*
            if (!double.TryParse(aryParams[7], out config.nPrice2))
            {

            }
            */
            string szPrice2 = aryParams[nPointer++];
            if ( szPrice2 != "" && config.itemConfig.nType == (int)eItemType.skill_point )
            {
                string[] aryPrice2 = szPrice2.Split( '_' );
                config.nPrice2 = double.Parse(aryPrice2[0]);
                config.fRisePricePercent = float.Parse(aryPrice2[1]);
            }


            if (!int.TryParse(aryParams[nPointer++], out config.nNum))
            {
                config.nNum = 1;
            }



            if (!int.TryParse(aryParams[nPointer++], out config.nOn))
            {
                config.nOn = 0;
            }


            config.szDesc = aryParams[nPointer++];


            List<sShoppingMallItemConfig> lst = null;
            if ( !m_dicShoppingMallItems.TryGetValue( (eItemType)config.itemConfig.nType, out lst ) )
            {
                lst = new List<sShoppingMallItemConfig>();
            }
            lst.Add( config );

            m_dicShoppingMallItems[(eItemType)config.itemConfig.nType] = lst;

        } // end for i





    } // end IEnumerator LoadConfig_Shoppingmall_New(string szFileName)



    IEnumerator LoadConfig_Shoppingmall(string szFileName)
    {

        WWW www = new WWW(szFileName);
        yield return www; //  待下载
        /*
        string[] aryLines = www.text.Split('\n');
        for (int i = 1; i < aryLines.Length; i++)
        {
            string[] aryParams = aryLines[i].Split(',');
            int nId = 0;
            if (!int.TryParse(aryParams[0], out nId))
            {
                continue;
            }
           
            sShoppingMallItemConfig config = new sShoppingMallItemConfig();
           
            int nItemType = ItemSystem.s_Instance.GetItemConfigById(nId).nType;
            List<sShoppingMallItemConfig> lst = GetItemListByType(nItemType);

          
            
            config.nId = nId;
            config.nPriceType1 = -1;  // “货币类型”默认值不能是0， 0是有效的货币类型
            config.nPriceType2 = -1;
            config.nPriceSubType1 = 0;
            config.nPriceSubType2 = 0;
            config.nPrice1 = 0;
            config.nPrice2 = 0;

            if (!int.TryParse(aryParams[1], out config.nPriceType1))
            {
                config.nPriceType1 = -1;
            }

            if (!int.TryParse(aryParams[2], out config.nPriceSubType1))
            {

            }

            if (!double.TryParse(aryParams[3], out config.nPrice1))
            {

            }

            if (!int.TryParse(aryParams[4], out config.nNum1))
            {
                config.nNum1 = 1;
            }


            if ( !int.TryParse(aryParams[5], out config.nPriceType2) )
            {
                config.nPriceType2 = -1;
               // Debug.Log( "没有price 2" );
            }

            if (!int.TryParse(aryParams[6], out config.nPriceSubType2))
            {

            }

            if (!double.TryParse(aryParams[7], out config.nPrice2))
            {

            }

            if (!int.TryParse(aryParams[8], out config.nNum2))
            {
                config.nNum2 = 1;
            }



            if ( !int.TryParse(aryParams[9], out config.nOn) )
            {
                config.nOn = 0;
            }

            if (!float.TryParse(aryParams[10], out config.fPriceRiseEachBuy))
            {
                config.fPriceRiseEachBuy = 0;
            }


            lst.Add(config); // 此处有一个完全不可理喻的现象，稍后有时间了再来深究 poppin to do
           
        } // end for i

        int nCurTotalRow = 0;
        int nCurTypeContainerNum = 0;
        foreach ( KeyValuePair<int, List<sShoppingMallItemConfig>> pair in m_dicShoppingMallItems )
        {
            List<sShoppingMallItemConfig> lst = pair.Value;

            // type_container
            ShoppinMallTypeContainer container = NewTypeContainer();

            string szTypeName = GetTypeNameById ( pair.Key ); // 这个key是道具类型，不是道具id
            container._txtTypeName.text = szTypeName;
            container.transform.SetParent(m_goContainerAllType.transform);
            vecScale.x = 1f;
            vecScale.y = 1f;
            vecScale.z = 1f;
            container.transform.localScale = vecScale;

            vecTempPos = m_vecTypeContainerPos;
            vecTempPos.y -= (nCurTypeContainerNum * m_fTypeContainerTitleHeight + nCurTotalRow * m_fStandardCounterHeight);
            container.transform.localPosition = vecTempPos;
            nCurTypeContainerNum++;
            // end type_container

            for (int i = 0; i < lst.Count; i++ )
            {
                sShoppingMallItemConfig config = lst[i];

                if (config.nOn == 0)
                {
                    continue;
                }

                UIItem item = NewBuyCounter();
                container.AddItem(item);

                ItemSystem.sItemConfig item_config = ItemSystem.s_Instance.GetItemConfigById( config.nId );
                item.SetConfig(config);

                item.SetPriceVisible(true);


                // 如果商品是“天赋点”，则购买价格和购买规则走专门的支线流程
                //if (item_config.nType == (int)eItemType.skill_point ) // 天赋点
                //{
                  //  item.SetPrice_TalentPoint(int.Parse(item_config.szParams));     
                //}
                //else
                //{
                    item.SetPrice(config);
                //}

                item._txtName.text = item_config.szName;

                item.SetUseEnalbed(false);
                item.SetNumVisible( false );
                item.SetLeftTimeVisible( false );

            } // end for i

            nCurTotalRow += container.GetRow();
        } // end foreach



        */
    }// end LoadConfig_Shoppingmall

   
    public void Init()
    {
    

        /*
        // poppin test
        int nCurTotalRow = 0;
        int nCurTypeContainerNum = 0;

        /// type_container 0
        ShoppinMallTypeContainer  container = NewTypeContainer();

        container.name = "钻石";
        container.transform.SetParent( m_goContainerAllType.transform);
        vecScale.x = 1f;
        vecScale.y = 1f;
        vecScale.z = 1f;
        container.transform.localScale = vecScale;

        vecTempPos = m_vecTypeContainerPos;
        vecTempPos.y -= ( nCurTypeContainerNum * m_fTypeContainerTitleHeight + nCurTotalRow * m_fStandardCounterHeight );
        container.transform.localPosition = vecTempPos;
        nCurTypeContainerNum++;

        UIItem item = NewBuyCounter();
        container.AddItem(item);
        item = NewBuyCounter();
       
        container.AddItem(item);
        item = NewBuyCounter();
         container.AddItem(item);
         item = NewBuyCounter();
         container.AddItem(item);
        item = NewBuyCounter();
        container.AddItem(item);
        item = NewBuyCounter();
        container.AddItem(item);
        item = NewBuyCounter();
        container.AddItem(item);

        nCurTotalRow += container.GetRow();



        /// type_container 1
        container = NewTypeContainer();
        container.name = "金币加成";
        container.transform.SetParent(m_goContainerAllType.transform);
        vecScale.x = 1f;
        vecScale.y = 1f;
        vecScale.z = 1f;
        container.transform.localScale = vecScale;

        vecTempPos = m_vecTypeContainerPos;
        vecTempPos.y -= ( nCurTypeContainerNum * m_fTypeContainerTitleHeight + nCurTotalRow * m_fStandardCounterHeight );
        container.transform.localPosition = vecTempPos;
        nCurTypeContainerNum++;

        item = NewBuyCounter();
        container.AddItem(item);
        item = NewBuyCounter();
        container.AddItem(item);
        item = NewBuyCounter();
        container.AddItem(item);
        item = NewBuyCounter();
        container.AddItem(item);
        nCurTotalRow += container.GetRow();
        */
    }

    public void OnClick_OpenShoppinMall()
    {
        _panelShoppingMall.SetActive( true );
        AccountSystem.s_Instance.SetGeneralMoneyCountersVisible( true );

        UIManager.s_Instance.OnOpenUI();
    }


    public void OnClick_CloseShoppinMall()
    {
        _panelShoppingMall.SetActive(false);
        m_bShoppinMallShowing = false;

        AccountSystem.s_Instance.SetGeneralMoneyCountersVisible(false);

        AudioManager.s_Instance.PlaySE_New(AudioManager.eSe_New.GuanBiJieMianAnNiu);

    }

    public bool IsShoppinMallShowing()
    {
        return m_bShoppinMallShowing;
    }

    public void OnClick_OpenSubPanelConfirmBuy()
    {
        _subpanelConfirmBuy.SetActive( true );

    }

    public void OnClick_CloseSubPanelConfirmBuy()
    {
        _subpanelConfirmBuy.SetActive(false);
    }

    public void BuySucceed( UIItem  item_to_buy, int nNum)
    {
        ItemSystem.sItemConfig item_config = ItemSystem.s_Instance.GetItemConfigById(item_to_buy.m_Config.nId);

        switch ((ShoppinMall.eItemType)item_config.nType)
        {
            //  会产生实体道具进入背包(跟策划约定：原则上实体道具都默认为1个，不会去配置多个)
            case ShoppinMall.eItemType.automobile_accelerate: //载具加速道具
            case ShoppinMall.eItemType.coin_raise: // 金币增收道具 
            case ShoppinMall.eItemType.fast_forward: // 瞬收道具 
            case ShoppinMall.eItemType.treassure_box: // 宝箱 
                {
                    // 生成一个道具，如果没有"立即使用“，则把该道具放进背包。
                    UIItem item = ItemSystem.s_Instance.CreateBagItem(item_to_buy);

                    // 是否立即使用
                    // to do

                    // 放进背包
                    ItemSystem.s_Instance.AddItem(item);
                }
                break;

            // 不产生实体道具，直接加数值
            case ShoppinMall.eItemType.skill_point: // 天赋点
                {
                    int nPlanetId = int.Parse(item_config.szParams);
                    int nCurNum = ScienceTree.s_Instance.GetSkillPoint((ScienceTree.eBranchType)nPlanetId);
                    nCurNum += nNum;
                    ScienceTree.s_Instance.SetSkillPoint( (ScienceTree.eBranchType)nPlanetId, nCurNum);
                }
                break;

        }; // end switch


    }


    public void OnClick_ConfirmBuy()
    {
        // 先判断钻石够不够
        double nCurDiamondNum = AccountSystem.s_Instance.GetGreenCash();
        if (nCurDiamondNum < m_nConfirmBuy_Price)
        {
            UIMsgBox.s_Instance.ShowMsg( "钻石不够" );

            // 链接到商城的钻石购买区 
            // to do

            return;
        }

        // 消耗钻石，购买成功
        nCurDiamondNum -= m_nConfirmBuy_Price;
        AccountSystem.s_Instance.SetGreenCash(nCurDiamondNum);

        BuySucceed( m_itemConfirmBuy, m_nConfirmBuy_Num );

      // UIMsgBox.s_Instance.ShowMsg( "购买成功" );
        _subpanelConfirmBuy.SetActive( false );

        return;

        if (_CurProcessingBuyUiItem.m_eItemType == eItemType.diamond)
        {
            UIMsgBox.s_Instance.ShowMsg( "此功能暂未开放" );
            return;
        }

        _subpanelConfirmBuy.SetActive(false);

        // 消耗货币
        switch (_CurProcessingBuyUiItem.m_ePriceType)
        {
            case ePriceType.green_cash:
                {
                    AccountSystem.s_Instance.SetGreenCash(AccountSystem.s_Instance.GetGreenCash() - _CurProcessingBuyUiItem.m_nPrice_Diamond);
                }
                break;
            case ePriceType.coin:
                {
                    int nPlanetId = (int)_CurProcessingBuyUiItem.m_ePriceSubType;
                    double nCurCoin = AccountSystem.s_Instance.GetCoin(nPlanetId);
                    AccountSystem.s_Instance.SetCoin(nPlanetId, nCurCoin - _CurProcessingBuyUiItem.m_nPrice_Coin);
                }
                break;

        } // end switch


        if (_CurProcessingBuyUiItem.m_eItemType == eItemType.skill_point)
        {
            UIMsgBox.s_Instance.ShowMsg( "购买技能点成功" );
            ScienceTree.eBranchType eType = (ScienceTree.eBranchType)_CurProcessingBuyUiItem.m_ePriceSubType;
            int nCurPoint = ScienceTree.s_Instance.GetSkillPoint(eType);
            ScienceTree.s_Instance.SetSkillPoint(eType, nCurPoint + 1);
            return;
        }

        _subpanelConfirmUse.SetActive( true );

   

        // 生成一个背包物品
        /*
        UIItem bag_item = ResourceManager.s_Instance.NewUiItem();
        bag_item.InitBagItem( _CurProcessingBuyUiItem );
        _CurProcessingUsingUiItem = bag_item;

        ItemSystem.s_Instance.AddItem(bag_item);
        */
        UIItemInBag bag_item = ResourceManager.s_Instance.NewUiItem();
        bag_item.InitBagItem(_CurProcessingBuyUiItem);
        _CurProcessingUsingUiItem = bag_item;
        _imgConfirmUse_Avatar.sprite = ResourceManager.s_Instance.m_aryCoinRaiseItemIcon[bag_item.m_nItemId];

        ItemSystem.s_Instance.AddItem(bag_item);
    }

    public void OnClick_CloseUsingImmediatelyPanel()
    {
        _subpanelConfirmUse.SetActive(false);
    }

    public void OnClick_UseImmediately()
    {
        ItemSystem.s_Instance.UseItem( _CurProcessingUsingUiItem );
        _subpanelConfirmUse.SetActive( false );

    }

    /// <summary>
    /// 资源管理
    /// </summary>
    /// <returns>The type container.</returns>
    /// 

    List<ShoppinMallTypeContainer> m_lstRecycledTypeContainers = new List<ShoppinMallTypeContainer>();
    public ShoppinMallTypeContainer NewTypeContainer()
    {
        ShoppinMallTypeContainer obj = null;

        if (m_lstRecycledTypeContainers.Count > 0)
        {
            obj = m_lstRecycledTypeContainers[0];
            obj.gameObject.SetActive( true );
            m_lstRecycledTypeContainers.RemoveAt(0);
            obj.Reset();
        }
        else
        {
            obj = GameObject.Instantiate(m_preTypeContainer).GetComponent<ShoppinMallTypeContainer>();
        }

        return obj;
    }

    public void DeleteTypeContainer(ShoppinMallTypeContainer obj )
    {
        obj.gameObject.SetActive( false );
        m_lstRecycledTypeContainers.Add( obj );
    }

    public GameObject m_containerRecycledItems;
    List<UIItem> m_lstRecycledBuyCounters = new List<UIItem>();
    public UIItem NewBuyCounter()
    {
        UIItem obj = null;

        if (m_lstRecycledBuyCounters.Count > 0)
        {
            obj = m_lstRecycledBuyCounters[0];
            obj.gameObject.SetActive( true );
            m_lstRecycledBuyCounters.RemoveAt(0);
        }
        else
        {
            obj = GameObject.Instantiate( m_preItemCounter ).GetComponent<UIItem>();
        }

        return obj;

    }

    public void DeleteBuyCounter(UIItem obj)
    {
        obj.gameObject.SetActive(false);
        m_lstRecycledBuyCounters.Add(obj);
        obj.transform.SetParent(m_containerRecycledItems.transform);
    }

    // 显示“确认购买”界面
    public void ShowConfirmBuyPanel( UIItem item, double nPrice, int nNum, sShoppingMallItemConfig config, ItemSystem.sItemConfig item_config )
    {
        m_itemConfirmBuy = item;
        m_nConfirmBuy_Price = nPrice;
        m_nConfirmBuy_Num = nNum;
        m_ConfirmBuyShoppingConfig = config;

        _txtConfirmBuy_ItemName.text = item_config.szName;
        _txtConfirmBuy_FuncAndValue.text = item_config.szDesc;
        _txtConfirmBuy_Cost.text = "花费 钻石 " + nPrice;

        _subpanelConfirmBuy.SetActive( true );
    }

    public void SetItemBuyTime( int nItemId, int nBuyTimes )
    {
        m_dicItemBuyTimes[nItemId] = nBuyTimes;
    }

    public int GetItemBuyTime(int nItemId)
    {
        int nBuyTimes = 0;
        if ( !m_dicItemBuyTimes.TryGetValue(nItemId, out nBuyTimes))
        {
            nBuyTimes = 0;
            m_dicItemBuyTimes[nItemId] = nBuyTimes;
        }
        return nBuyTimes;
    }


    /////// 非正式流程，只是为了应付这个检查点
    public Sprite[] m_aryAvatarBg;


    public Image _imgCurBuyCounterAvatar;
    public Image _imgCurBuyCounterAvatarBg;
    public Text _txtCurBuyCounterDuration;
    public Text _txtCurBuyCounterValue;


    UIShoppinAndItemCounter m_CurBuyCounter = null;
    int m_nBuyMoneyType = 0; // 0 - diamond  1 - coin
    public void ShowConfirmBuyPanel( UIShoppinAndItemCounter counter, int nBuyMoneyType )
    {
        m_CurBuyCounter = counter;
        m_nBuyMoneyType = nBuyMoneyType;

        _subpanelConfirmBuy.SetActive( true );

        _txtConfirmBuy_Cost.text = string.Format("花费       {0} 购买吗？", m_CurBuyCounter.m_Config.nPrice1);

        if ( m_CurBuyCounter.m_nItemType == 1 )
        {
            _txtConfirmBuy_FuncAndValue.text = string.Format(m_CurBuyCounter.m_szFuncDest, CyberTreeMath.FormatTime( m_CurBuyCounter.m_nDuration ), 1 + m_CurBuyCounter.m_fValue);
            _txtCurBuyCounterDuration.text = m_CurBuyCounter._txtDuration.text;
            _txtCurBuyCounterDuration.gameObject.SetActive( true );
            _txtCurBuyCounterValue.text = m_CurBuyCounter._txtValue.text;
            _txtCurBuyCounterValue.gameObject.SetActive( true );
            _imgCurBuyCounterAvatar.sprite = m_CurBuyCounter._imgAvatar.sprite;
            _imgCurBuyCounterAvatarBg.sprite = m_aryAvatarBg[1];
        }
        else if (m_CurBuyCounter.m_nItemType == 2)
        {
            _txtConfirmBuy_FuncAndValue.text = m_CurBuyCounter.m_szFuncDest;
            _imgCurBuyCounterAvatar.sprite = m_CurBuyCounter._imgAvatar.sprite;
            _imgCurBuyCounterAvatarBg.sprite = m_aryAvatarBg[2];
            _txtCurBuyCounterValue.gameObject.SetActive(false);
            _txtCurBuyCounterDuration.gameObject.SetActive(false);
        }

        _imgCurBuyCounterAvatar.sprite = m_CurBuyCounter._imgAvatar.sprite;
    }

    public void OnBuySkillPointSucceeded( sShoppingMallItemConfig config )
    {
        int nBuyNum = config.nNum;

        int nSkillPointType = config.itemConfig.aryIntParams[0];
        int nCurPoint = ScienceTree.s_Instance.GetSkillPoint((ScienceTree.eBranchType)nSkillPointType);

        int nCurBuyTimes = ScienceTree.s_Instance.GetSkillPointBuyTimes(nSkillPointType);
        nCurBuyTimes += nBuyNum;
        ScienceTree.s_Instance.SetSkillPointBuyTimes(nSkillPointType, nCurBuyTimes);

        ScienceTree.s_Instance.SetSkillPoint((ScienceTree.eBranchType)nSkillPointType, nCurPoint + nBuyNum);

        RefreshSkillPonitCounters();

        ScienceTree.s_Instance.SaveData_BuyTimes();
    }

    // confirm buy
    public void OnClick_ConfirmBuy_Test()
    {
   
            double nCurDiamond = AccountSystem.s_Instance.GetGreenCash();
            nCurDiamond -= m_CurBuyCounter.m_Config.nPrice1; // “Price1”是钻石价格，“ Price2”是金币价格
            AccountSystem.s_Instance.SetGreenCash(nCurDiamond);


        if ( m_CurBuyCounter.m_Config.itemConfig.nType == (int)eItemType.coin_raise ||
            m_CurBuyCounter.m_Config.itemConfig.nType == (int)eItemType.automobile_accelerate ||
            m_CurBuyCounter.m_Config.itemConfig.nType == (int)eItemType.fast_forward


           ) // 强化道具

        {
            ItemSystem.s_Instance.AddToItembag( m_CurBuyCounter.m_Config.nId );

            AudioManager.s_Instance.PlaySE_New(AudioManager.eSe_New.GouMaiDaoJu);
        }
        else if (m_CurBuyCounter.m_Config.itemConfig.nType ==  (int)eItemType.skill_point ) // 天赋点
        {
            OnBuySkillPointSucceeded(m_CurBuyCounter.m_Config);
            /*
            int nBuyNum = m_CurBuyCounter.m_Config.nNum;

            int nSkillPointType = m_CurBuyCounter.m_Config.itemConfig.aryIntParams[0];
            int nCurPoint = ScienceTree.s_Instance.GetSkillPoint((ScienceTree.eBranchType)nSkillPointType);
           
            int nCurBuyTimes = ScienceTree.s_Instance.GetSkillPointBuyTimes(nSkillPointType);
            nCurBuyTimes += nBuyNum;
            ScienceTree.s_Instance.SetSkillPointBuyTimes(nSkillPointType, nCurBuyTimes);

            ScienceTree.s_Instance.SetSkillPoint((ScienceTree.eBranchType)nSkillPointType, nCurPoint + nBuyNum);

            RefreshSkillPonitCounters();

            ScienceTree.s_Instance.SaveData_BuyTimes();
            */
            AudioManager.s_Instance.PlaySE_New(AudioManager.eSe_New.GouMaiJiNengDian);
        }

        _subpanelConfirmBuy.SetActive(false);

    //   AudioManager.s_Instance.PlaySE(AudioManager.eSE.e_buy_plane);
    }


    Dictionary<eItemType, List<UIShoppinAndItemCounter>> m_dicRecycledCounters = new Dictionary<eItemType, List<UIShoppinAndItemCounter>>();
    List<UIShoppinAndItemCounter> m_lstCurShowingItems = new List<UIShoppinAndItemCounter>();
    public UIShoppinAndItemCounter NewCounter(eItemType item_type)
    {
        UIShoppinAndItemCounter counter = null;
        List<UIShoppinAndItemCounter> lst = null;
        if ( !m_dicRecycledCounters.TryGetValue(item_type, out lst) )
        {
            lst = new List<UIShoppinAndItemCounter>();
        }
        if ( false/*lst.Count > 0*/ )
        {
            counter = lst[0];
            counter.gameObject.SetActive( true );
            lst.RemoveAt(0);
        }
        else
        {
            counter = GameObject.Instantiate(m_aryCounterPrefab[(int)item_type]).GetComponent<UIShoppinAndItemCounter>();
        }
        m_dicRecycledCounters[item_type] = lst;
        counter.SetItemType(item_type);
        return counter;
    }

    public void DeleteCounter(UIShoppinAndItemCounter counter)
    {
        counter.transform.SetParent(m_containerRecycledItems.transform);
        GameObject.Destroy(counter.gameObject);

        return;
        eItemType item_type = counter.GetItemType();

        List<UIShoppinAndItemCounter> lst = null;
        if (!m_dicRecycledCounters.TryGetValue(item_type, out lst))
        {
            lst = new List<UIShoppinAndItemCounter>();
        }

        counter.gameObject.SetActive( false );
        lst.Add( counter );
        counter.transform.SetParent( m_containerRecycledItems.transform );
        m_dicRecycledCounters[item_type] = lst;
    }

    Dictionary<eItemType, int> m_dicShoppinTypes = new Dictionary<eItemType, int>();
    public void OnClick_OpenShoppinMall_SkillPoint()
    {
        m_dicShoppinTypes.Clear();
        m_dicShoppinTypes[eItemType.skill_point] = 1;


        OpenShoppinMall_AllTypes(ref m_dicShoppinTypes);
    }


    public void OnClick_OpenShoppinMall_Diamond()
    {
        m_dicShoppinTypes.Clear();
        m_dicShoppinTypes[eItemType.diamond] = 1;


        OpenShoppinMall_AllTypes(ref m_dicShoppinTypes);

       // AudioManager.s_Instance.PlaySE_New(AudioManager.eSe_New.ZuanShiGouMaiJieMianAnNiu);

    }

    public void OnClick_OpenShoppinMall_MainUI()
    {
        m_dicShoppinTypes.Clear();
        m_dicShoppinTypes[eItemType.automobile_accelerate] = 1;
        m_dicShoppinTypes[eItemType.coin_raise] = 1;
        m_dicShoppinTypes[eItemType.fast_forward] = 1;

        OpenShoppinMall_AllTypes( ref m_dicShoppinTypes);

        UIManager.s_Instance.OnOpenUI();

        AudioManager.s_Instance.PlaySE_New(AudioManager.eSe_New.ShangChengAnNiu);
    }

    // right here
    List<ShoppinMallTypeContainer> m_lstAllShowingTypeContainers = new List<ShoppinMallTypeContainer>();
    ShoppinMallTypeContainer m_TypeContainerOfSkillPoint = null;
    bool m_bShoppinMallShowing = false;
    public void OpenShoppinMall_AllTypes( ref Dictionary<eItemType, int> dicTypes )
    {
        AudioManager.s_Instance.PlaySE_New(AudioManager.eSe_New.ZuanShiGouMaiJieMianAnNiu);

        AccountSystem.s_Instance.SetGeneralMoneyCountersVisible(true);

        m_bShoppinMallShowing = true;

        _panelShoppingMall.SetActive(true);

        ClearShowingList();

        int nCurTotalRow = 0;
        int nCurTypeContainerNum = 0;
        float fCurTotalHeight = 0;
        float fLastContainerHeight = 0f;
        vecTempPos = m_vecTypeContainerPos;

        float fTotalHeight = 0;

        foreach ( KeyValuePair<eItemType, List<sShoppingMallItemConfig>> pair in m_dicShoppingMallItems )
        {
            int nTemp = 0;
            if ( !dicTypes.TryGetValue( pair.Key, out nTemp) )
            {
                continue;
            }

            List<sShoppingMallItemConfig> lst = pair.Value;

            ShoppinMallTypeContainer container = NewTypeContainer();

            if ( pair.Key == eItemType.skill_point )
            {
                m_TypeContainerOfSkillPoint = container;
            }

            m_lstAllShowingTypeContainers.Add(container);
            string szTypeName = GetTypeNameById((int)pair.Key); // 这个key是道具类型，不是道具id             container._txtTypeName.text = szTypeName;             container.transform.SetParent(m_goContainerAllType.transform);             vecScale.x = 1f;             vecScale.y = 1f;             vecScale.z = 1f;             container.transform.localScale = vecScale;
            nCurTypeContainerNum++;
            /*
            vecTempPos = m_vecTypeContainerPos;
            float fHeightOfThisGroup = (nCurTypeContainerNum * m_fTypeContainerTitleHeight + nCurTotalRow * GetCounterHeight(pair.Key));
            vecTempPos.y -= fHeightOfThisGroup;             container.transform.localPosition = vecTempPos;
            fCurTotalHeight += fHeightOfThisGroup;
*/
            if (pair.Key == ShoppinMall.eItemType.fast_forward)
            {
        
            }

            for (int i = 0; i < lst.Count; i++)
            {
                sShoppingMallItemConfig config = lst[i];                  if (config.nOn == 0)                 {                     continue;                 }                  UIShoppinAndItemCounter counter = NewCounter((eItemType)config.itemConfig.nType);                 container.AddItem(counter);                 counter.Init( config ); 
            } // end for i

            nCurTotalRow += container.GetRow();



            vecTempPos.y -= fLastContainerHeight;
            container.transform.localPosition = vecTempPos;
            fLastContainerHeight = container.CalculateHeight( pair.Key );
            fTotalHeight += fLastContainerHeight;

        } // end foreach

        _vlg.padding.bottom =(nCurTotalRow  + 1 )* 500 ;//(int)(fTotalHeight);     
    }

    public void ClearShowingList()
    {
        for (int i = m_lstAllShowingTypeContainers.Count - 1; i >= 0; i-- )
        {
            ShoppinMallTypeContainer container = m_lstAllShowingTypeContainers[i];
            container.ClearAllItems();
            DeleteTypeContainer(container);
            m_lstAllShowingTypeContainers.RemoveAt(i);
        }

        m_lstAllShowingTypeContainers.Clear();
    }

    public void AddOneItemToShowingList(UIShoppinAndItemCounter counter)
    {

    }

    public float GetCounterHeight( eItemType item_type )
    {
        if ( item_type == eItemType.skill_point )
        {
            return m_fStandardCounterHeight_Long;
        }
        return m_fStandardCounterHeight;
    }

    public void RefreshSkillPonitCounters()
    {
        m_TypeContainerOfSkillPoint.RefreshPrice();
    }



} // end class
