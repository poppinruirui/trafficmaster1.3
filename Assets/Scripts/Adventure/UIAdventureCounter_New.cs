﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIAdventureCounter_New : MonoBehaviour {

    /// <summary>
    /// UI
    /// </summary>
    public UIAdventureRewardCounter[] m_aryRewardCounters;
    public Text _txtTitle;
    public Text _txtTotalTime;
    public Text _txtName;

    public Text _txtReduceTime;
    public Text _txtReduceTime_ColdDown;
    public GameObject _containerPlay;

    public Image _imgQualityBg;

    public AdventureManager_New.sAdventureConfig m_GeneralConfig = new AdventureManager_New.sAdventureConfig();
    public AdventureManager_New.sRewardConfig m_MainRewardConfig = new AdventureManager_New.sRewardConfig();
    public List<AdventureManager_New.sRewardConfig> m_lstViceRewardConfig = new List<AdventureManager_New.sRewardConfig>();
    public int m_nViceNum = 0;

    public int m_nCounterIndex = 0;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnClick_Begin()
    {
        AdventureManager_New.s_Instance.BeginAdventure( this );

        AudioManager.s_Instance.PlaySE_New(AudioManager.eSe_New.KaiShiTanXianAnNiu);
    }

    public void OnClick_WatchAdsReduceTime()
    {
        if (AdventureManager_New.s_Instance.IsWatchAdsColdDowning())
        {
            UIMsgBox.s_Instance.ShowMsg( "功能冷却中" );
            return;
        }

        AdventureManager_New.s_Instance.WatchAdsReduceTime();
    }
 

    public void UpdateInfo(  AdventureManager_New.sAdventureConfig config,  AdventureManager_New.sRewardConfig main_reward,  List<AdventureManager_New.sRewardConfig> lstViceReward, int nViceNum )
    {
        m_nViceNum = nViceNum;
        m_GeneralConfig = config;

        AdventureManager_New.CopyRewardData( ref main_reward, ref m_MainRewardConfig);
     

        for (int i = 0; i < nViceNum; i++)
        {
            AdventureManager_New.sRewardConfig vice_reward;
            if (m_lstViceRewardConfig.Count <= i)
            {
                vice_reward = new AdventureManager_New.sRewardConfig();
                m_lstViceRewardConfig.Add(vice_reward);
            }
            else
            {
                vice_reward = m_lstViceRewardConfig[i];
            }
            AdventureManager_New.sRewardConfig reward_in_list = lstViceReward[i];
            AdventureManager_New.CopyRewardData( ref reward_in_list, ref vice_reward);
            m_lstViceRewardConfig[i] = vice_reward;
        }

        for (int i = 0; i < m_aryRewardCounters.Length; i++ )
        {
            m_aryRewardCounters[i].gameObject.SetActive( false );
        }

        int nIndex = (int)config.eQuality - 3;
        _txtTitle.text = config.szDesc;
        _txtTitle.color = AdventureManager_New.s_Instance.m_aryAdventureQualityColor[nIndex];
        _txtName.text = AdventureManager_New.s_Instance.m_aryAdventureName[nIndex];
        _txtTotalTime.text = CyberTreeMath.FormatTime((int)config.fDuration);
        _imgQualityBg.sprite = AdventureManager_New.s_Instance.m_aryAdventureQualityBg[nIndex];

     //   m_MainRewardConfig.nNum = CyberTreeMath.GetRandomValue(m_MainRewardConfig.nMinNum, m_MainRewardConfig.nMaxNum + 1);
        UIAdventureRewardCounter main_reward_counter = m_aryRewardCounters[0];
        main_reward_counter.gameObject.SetActive( true );
        main_reward_counter._txtNum.text = m_MainRewardConfig.nNum.ToString();

        ItemSystem.sItemConfig item_config = ItemSystem.s_Instance.GetItemConfigById(m_MainRewardConfig.nItemId);
        main_reward_counter._txtDesc.text = item_config.szDesc;
        main_reward_counter._imgAvatar.sprite = ItemSystem.s_Instance.GetItemIconById(m_MainRewardConfig.nItemId);

        int nViceRewardCounterIndex = 1;
        for (int i = 0; i < m_nViceNum; i++ )
        {
            AdventureManager_New.sRewardConfig vice_reward = m_lstViceRewardConfig[i];
     //       vice_reward.nNum = CyberTreeMath.GetRandomValue(vice_reward.nMinNum, vice_reward.nMaxNum + 1);
            UIAdventureRewardCounter vice_reward_counter = m_aryRewardCounters[nViceRewardCounterIndex++];
            vice_reward_counter.gameObject.SetActive( true );
            vice_reward_counter._txtNum.text = vice_reward.nNum.ToString();
            item_config = ItemSystem.s_Instance.GetItemConfigById(vice_reward.nItemId);
            vice_reward_counter._txtDesc.text = item_config.szDesc;;
            vice_reward_counter._imgAvatar.sprite = ItemSystem.s_Instance.GetItemIconById(vice_reward.nItemId);
            m_lstViceRewardConfig[i] = vice_reward;

        }

    }

    public void Init(UIAdventureCounter_New counter )
    {
        UpdateInfo(  counter.m_GeneralConfig,  counter.m_MainRewardConfig,  counter.m_lstViceRewardConfig, counter.m_nViceNum);
    }


} // end class
