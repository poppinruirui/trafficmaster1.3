﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class ItemSystem : MonoBehaviour {

    public static Vector3 vecTempScale = new Vector3();

    public UIItemBagContainer _theBag;

    public Sprite[] m_aryItemIcon;

    public UIShoppinAndItemCounter[] m_aryShoppinCounter;

    public static ItemSystem s_Instance = null;

    public GameObject _containerRecycledItems;

    public GameObject _panelItemBag;

    public GameObject _containerUsing;
    public GameObject _containerNotUsing;

    List<UIItemInBag> m_lstUsingItems = new List<UIItemInBag>();
    List<UIItem> m_lstNowUsingItems = new List<UIItem>();

    public struct sItemConfig
    {
        public int nId; // id
        public string szName; //  名称
        public int nType;     // 道具类型 
        public int nResId;    // 美术资源Id
        public string szParams; // 参数列表 
        public int nDuration;  // 持续时间
        public string szDesc;   // 描述

        public int[] aryIntParams;
        public float[] aryFloatParams;

    };
    sItemConfig tempItemConfig;

    Dictionary<int, sItemConfig> m_dicItemConfig = new Dictionary<int, sItemConfig>();

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start()
    {


        string szConfigFileName_Item = DataManager.url + "item.csv";
        StartCoroutine(LoadConfig_Item(szConfigFileName_Item));
      //  LoadConfigOffline_Item(szConfigFileName_Item);
    } // end start
    

    public void LoadConfigOffline_Item(string szFileName)
    {
        StreamReader sr = new StreamReader(szFileName);
        string szAll = sr.ReadToEnd();         string[] aryLines = szAll.Split('\n');
        for (int i = 1; i < aryLines.Length; i++)
        {
            string[] aryParams = aryLines[i].Split(',');

            int nId = 0;
            if (!int.TryParse(aryParams[0], out nId))
            {
                continue;
            }

            sItemConfig config = new sItemConfig();
            config.nId = nId;
            config.szName = aryParams[1];
            config.nType = int.Parse(aryParams[2]);
            config.nResId = int.Parse(aryParams[3]);
            config.szParams = aryParams[4];
            int.TryParse(aryParams[5], out config.nDuration);
            config.szDesc = aryParams[6];

            if (config.nType == (int)ShoppinMall.eItemType.skill_point)
            {
                config.aryIntParams = new int[1];
                config.aryIntParams[0] = int.Parse(config.szParams);
            }
            else if (config.nType == (int)ShoppinMall.eItemType.coin_raise ||
                     config.nType == (int)ShoppinMall.eItemType.automobile_accelerate ||
                     config.nType == (int)ShoppinMall.eItemType.fast_forward
                    )
            {
                config.aryFloatParams = new float[1];
                config.aryFloatParams[0] = float.Parse(config.szParams);
            }

            m_dicItemConfig[nId] = config;
        } // end for i


        // item.txt文件加载完成之后才开始加载shoppingmall.txt文件，因为后者在解析的过程中要依托前者的配置。
        ShoppinMall.s_Instance.LoadConfig();


        m_bItemConfigLoaded = true;
        DataManager.s_Instance.TryLoadMyDataCurTrackPlanesData();


    } // end LoadConfigOffline_Item


    public bool m_bItemConfigLoaded = false;
    IEnumerator LoadConfig_Item(string szFileName)
    {
        WWW www = new WWW(szFileName);
        yield return www; // 等待下载

        string[] aryLines = www.text.Split('\n');
        for (int i = 1; i < aryLines.Length; i++)
        {
            string[] aryParams = aryLines[i].Split(',');

            int nId = 0;
            if (!int.TryParse(aryParams[0], out nId))
            {
                continue;
            }

            sItemConfig config = new sItemConfig();
            config.nId = nId;
            config.szName = aryParams[1];
            config.nType = int.Parse(aryParams[2]);
            config.nResId = int.Parse(aryParams[3]);
            config.szParams = aryParams[4];
            int.TryParse(aryParams[5], out config.nDuration);
            config.szDesc = aryParams[6];

            if ( config.nType == (int)ShoppinMall.eItemType.skill_point )
            {
                config.aryIntParams = new int[1];
                config.aryIntParams[0] = int.Parse(config.szParams );
            }
            else if (config.nType == (int)ShoppinMall.eItemType.coin_raise||
                     config.nType == (int)ShoppinMall.eItemType.automobile_accelerate ||
                     config.nType == (int)ShoppinMall.eItemType.fast_forward)
            {
                config.aryFloatParams = new float[1];
                config.aryFloatParams[0] = float.Parse(config.szParams);
            }

            m_dicItemConfig[nId] = config;
        } // end for i


        // item.txt文件加载完成之后才开始加载shoppingmall.txt文件，因为后者在解析的过程中要依托前者的配置。
        ShoppinMall.s_Instance.LoadConfig();


        m_bItemConfigLoaded = true;
        DataManager.s_Instance.TryLoadMyDataCurTrackPlanesData();

    } // end LoadConfig_Item

    public sItemConfig GetItemConfigById( int nItemId )
    {
        if (!m_dicItemConfig.TryGetValue(nItemId, out tempItemConfig))
        {
//            Debug.LogError("GetItemConfigById=" + nItemId);
        }

        return tempItemConfig;
    }

    // Update is called once per frame
    float m_fTimeElapse = 0f;
	void Update () {
        CountingLoop();
	}

    void CountingLoop()
    {
        // 每秒一次轮询
        m_fTimeElapse += Time.deltaTime;
        if (m_fTimeElapse < 1f)
        {
            return;
        }
        m_fTimeElapse = 0;

        /*
        for (int i = m_lstUsingItems.Count - 1; i >= 0; i-- ) // 如果中途可能删除节点，则应该反向遍历列表
        {
            UIItemInBag item = m_lstUsingItems[i];
            if ( item.DoCount() )
            {
                m_lstUsingItems.Remove( item );
                ResourceManager.s_Instance.DeleteUiItem( item );
                Main.s_Instance.UpdateRaise();
                continue;
            }

        } // end for 
        */

        /*
        for (int i = m_lstNowUsingItems.Count - 1; i >= 0; i-- )
        {
            UIItem item = m_lstNowUsingItems[i];
            if ( item.Counting() )
            {
                m_lstNowUsingItems.Remove( item );
                ShoppinMall.s_Instance.DeleteBuyCounter( item );

                DoSomethingWhenItemEnd();

                continue;
            }


        } // end for i
        */

        for (int i = m_lstUsingItems_New.Count - 1; i >= 0; i-- )
        {
            UIShoppinAndItemCounter item_using = m_lstUsingItems_New[i];
            item_using.Loop();


        }

    }

    public void DoSomethingWhenItemEnd()
    {
        Main.s_Instance.UpdateRaise();
        MapManager.s_Instance.CalculateAllPlanesSpeed();
    }

    public void OnClickButton_OpenBag()
    {
        _panelItemBag.SetActive(true);

        UIManager.s_Instance.OnOpenUI();

        AudioManager.s_Instance.PlaySE_New(AudioManager.eSe_New.DaoJuAnNiu);

    }


    public void OnClickButton_CloseBag()
    {
        _panelItemBag.SetActive( false );

        AudioManager.s_Instance.PlaySE_New(AudioManager.eSe_New.GuanBiJieMianAnNiu);
    }

    public void AddItem( UIItemInBag item )
    {
        item.transform.SetParent(_containerNotUsing.transform);
        vecTempScale.x = 1.3f;
        vecTempScale.y = 1.3f;
        vecTempScale.z = 1f;
        item.transform.localScale = vecTempScale;
    }

    public UIItem SearchSameIdUsing( UIItem item)
    {
        for (int i = 0; i < m_lstNowUsingItems.Count; i++ )
        {
            if ( item.GetId() == m_lstNowUsingItems[i].GetId())
            {
                return m_lstNowUsingItems[i];
            }
        }

        return null;
    }
    
    public void AddToUsingList( UIItem item )
    {
        m_lstNowUsingItems.Add( item );
        item.SetUseEnalbed( false );
        item.SetPriceVisible( false );
        item.SetLeftTimeVisible( true );
        item.SetNumVisible(false );
      //  item.SetLeftTime();
        item.transform.SetParent( _containerUsing.transform );
        vecTempScale.x = 1f;
        vecTempScale.y = 1f;
        vecTempScale.z = 1f;
        item.transform.localScale = vecTempScale;
    }

    public List<UIItem> GetUsingItemList()
    {
        return m_lstNowUsingItems;
    }

    public void UseItem( UIItem item )
    {
        // 如果是瞬时性道具，则直接体现结果即可。
        // 如果是持续性道具，则去“正在使用”列表中查询，有没有相同Id号的道具正在使用；如有，则直接累加时间，不产生新的使用效果
        bool bNeedNewItem = false;
        switch ((ShoppinMall.eItemType)item.m_BagItemConfig.nType)
        {
            case ShoppinMall.eItemType.coin_raise:
            case ShoppinMall.eItemType.automobile_accelerate:
                {
                    bNeedNewItem = true;
                }
                break;

        } // end switch

        // 生成一个新的Item，用于添加进“正在使用”列表。
        UIItem item_using = null;
        if (bNeedNewItem)
        {
            UIItem exist_item = SearchSameIdUsing(item);
            if (exist_item != null)
            {
                bNeedNewItem = false;

                int nLeftTime = exist_item.GetLeftTime();
                nLeftTime += item.m_BagItemConfig.nDuration;
                exist_item.SetLeftTime(nLeftTime);
            }
            else
            {
                item_using = item.Clone();
            }
           


        }



        switch ( (ShoppinMall.eItemType)item.m_BagItemConfig.nType )
        {
            case ShoppinMall.eItemType.coin_raise:
                {
                    if (bNeedNewItem)
                    {
                        float fRaise = float.Parse(item.m_BagItemConfig.szParams);
                        item_using.SetLeftTime(item.m_BagItemConfig.nDuration);
                        item_using.SetFloatParam(0, fRaise);
                        AddToUsingList(item_using);

                        Main.s_Instance.UpdateRaise();
                    }
                }
                break;
            case ShoppinMall.eItemType.automobile_accelerate:
                {
                    if (bNeedNewItem)
                    {
                        float fRaise = float.Parse(item.m_BagItemConfig.szParams);
                        item_using.SetLeftTime(item.m_BagItemConfig.nDuration);
                        item_using.SetFloatParam(0, fRaise);
                        AddToUsingList(item_using);

                        MapManager.s_Instance.CalculateAllPlanesSpeed();
                    }
                }
                break;
            case ShoppinMall.eItemType.fast_forward:
                {
                    // 获取当前赛道的DPS
                    int nDPS = (int)MapManager.s_Instance.GetCurDistrict().CalculateDPS();
                    int nGainTime = int.Parse(item.m_BagItemConfig.szParams);
                    int nGain = nDPS * nGainTime;
                    string szResult = nDPS + "DPS X" + nGainTime + "秒 =" +  nGain;
                    UIMsgBox.s_Instance.ShowMsg(szResult);

                    int nCurPlanetId = MapManager.s_Instance.GetCurPlanet().GetId();
                    double nCurCoin = AccountSystem.s_Instance.GetCoin(nCurPlanetId);
                    AccountSystem.s_Instance.SetCoin(nCurPlanetId, nCurCoin + nGain);

                }
                break;
            case ShoppinMall.eItemType.treassure_box:
                {
                    TreasureBoxManager.s_Instance.OpenTreasureBox(item);
                }
                break;

        } // end switch


        // 如果当前数量为1，使用之后背包中的道具就消失；如果当前数量大于1，则该道具数量减1，不消失。
        int nCurNum = item.GetNum();
        if (nCurNum > 1)
        {
            item.SetNum(nCurNum - 1);
        }
        else
        {
            ShoppinMall.s_Instance.DeleteBuyCounter(item);
        }

    }

    public void AddItem(UIItem item)
    {
        // 先判断背包中有没有同Id的道具，如果有，就加该道具的数量，而不用重复占一格
        foreach( Transform child in _containerNotUsing.transform )
        {
            UIItem exist_item = child.gameObject.GetComponent<UIItem>();
            if ( exist_item.GetBagItemId() == item.GetBagItemId())
            {
                exist_item.SetNum(exist_item.GetNum() + 1);
                ShoppinMall.s_Instance.DeleteBuyCounter(item);
                return;
            }
        }

        item.SetUseEnalbed( true );
        item.SetLeftTimeVisible( false );
        item.SetNumVisible( true );
        item.SetNum(1);
        item.transform.SetParent(_containerNotUsing.transform);
        vecTempScale.x = 1.3f;
        vecTempScale.y = 1.3f;
        vecTempScale.z = 1f;
        item.transform.localScale = vecTempScale;
    }

    public List<UIItemInBag> GetItemList()
    {
        return m_lstUsingItems;
    }

    public void UseItem(UIItemInBag item)
    {

        for (int i = 0; i < m_lstUsingItems.Count; i++ )
        {
            UIItemInBag item_using = m_lstUsingItems[i];
            if (item_using.m_nItemId == item.m_nItemId)
            {
                item_using.m_nDuration += item.m_nDuration;
                ResourceManager.s_Instance.DeleteUiItem(item);     
               
                return;
            }
        } // end foreach


//        item.SetCountingPanelVisible( true );
//        item.SetUseButtonVisible( false );
        item.m_StartTime = Main.GetSystemTime();
        m_lstUsingItems.Add(item);
        item.transform.SetParent(_containerUsing.transform);



        Main.s_Instance.UpdateRaise();


    }

    public UIItem CreateBagItem( UIItem item_to_buy )
    {
        UIItem item = item_to_buy.Clone();//ShoppinMall.s_Instance.NewBuyCounter();

        item.SetPriceVisible( false );

 

        return item;
    }
     
    public float GetAutomobileSpeedAccelerateByItem()
    {
        float fRaise = 0;

        for (int i = 0; i < m_lstNowUsingItems.Count; i++ )
        {
            UIItem item = m_lstNowUsingItems[i];
            if ( item.m_BagItemConfig.nType != (int)ShoppinMall.eItemType.automobile_accelerate )
            {
                continue;
            }
            fRaise += item.GetFloatParam(0);
        }

        return fRaise;
    }

    //////// 这个流程不是很正规，只是为了应付12月10号的检查点
    public UIShoppinAndItemCounter AddToItembag( int nId, int nNum = 1 )
    {
        sItemConfig item_config = GetItemConfigById( nId );
        if ( item_config.nType == (int)ShoppinMall.eItemType.skill_point )
        {
            ScienceTree.eBranchType eSkillPointType = (ScienceTree.eBranchType)item_config.aryIntParams[0];
            int nCurSkillPoint = ScienceTree.s_Instance.GetSkillPoint(eSkillPointType);
            nCurSkillPoint += nNum;
            ScienceTree.s_Instance.SetSkillPoint(eSkillPointType, nCurSkillPoint);

            return null;
        }else if (item_config.nType == (int)ShoppinMall.eItemType.diamond)
        {
            AccountSystem.s_Instance.SetGreenCash(AccountSystem.s_Instance.GetGreenCash() + nNum);
            return null;
        }

        // 先判断背包中有没有相同Id的道具，有就加个数即可；没有才实例化一个道具出来
        bool bExist = false;
        /*
        foreach( Transform child in _containerNotUsing.transform)
        {
            UIShoppinAndItemCounter child_item = child.gameObject.GetComponent<UIShoppinAndItemCounter>();
            if ( nId == child_item.m_nItemId)
            {
                child_item.SetNum(child_item.GetNum() + nNum);

                bExist = true;
                break;
            }
        }
        */


        UIShoppinAndItemCounter item_counter = _theBag.AddItem(nId, nNum);
        if (item_counter == null)
        {
            item_counter = ShoppinMall.s_Instance.NewCounter((ShoppinMall.eItemType)item_config.nType);//ResourceManager.s_Instance.NewShoppinAndItemCounter();
            item_counter.transform.SetParent(_containerNotUsing.transform);
            vecTempScale.x = 1f;
            vecTempScale.y = 1f;
            vecTempScale.z = 1f;
            item_counter.transform.localScale = vecTempScale;
            item_counter.SetItemInBag(true);
            item_counter.CopyData(nId);
            item_counter.SetNum(nNum);

            _theBag.AddItem(item_counter);
        }

        SaveData();

        return item_counter;
    }

    public void RemoveItem(UIShoppinAndItemCounter counter)
    {
        _theBag.RemoveOneItem(counter);
        ResourceManager.s_Instance.DeleteShoppinAndItemCounter(counter);
        counter.transform.SetParent(null); // 暂定
        int nNUm = _containerUsing.transform.childCount;
        SaveData();
    }

    public void RemoveFromUsingList(UIShoppinAndItemCounter counter)
    {
        m_lstUsingItems_New.Remove(counter);
    }

    public UIShoppinAndItemCounter UseItem(UIShoppinAndItemCounter item )
    {
        UIShoppinAndItemCounter the_using_item = null;

        // 先判断有无完全一样的道具正在运行中，如果有，就累加时间，而不是新增一个该道具的使用效果
        bool bExist = false;
        foreach( Transform child in _containerUsing.transform)
        {
            UIShoppinAndItemCounter node_item = child.gameObject.GetComponent<UIShoppinAndItemCounter>();

            bool bSameValueCoinPromoteItem = false;
            if ( item.m_nItemType == (int)ShoppinMall.eItemType.coin_raise &&
                node_item.m_nItemType == (int)ShoppinMall.eItemType.coin_raise &&
                item.m_fValue == node_item.m_fValue


               )
            {
                bSameValueCoinPromoteItem = true;
            }

            if ( node_item.m_nItemId == item.m_nItemId || bSameValueCoinPromoteItem

               )
            {
                node_item.AddTime( item.m_nDuration );
                the_using_item = node_item;
                bExist = true;
                break;
            }
        }

        bool bDoNotCreateItem = false;
        if ( (ShoppinMall.eItemType)item.m_nItemType == ShoppinMall.eItemType.fast_forward )
        {
            bDoNotCreateItem = true;



        }


        if ((ShoppinMall.eItemType)item.m_nItemType == ShoppinMall.eItemType.fast_forward)
        {
            double fDPS = MapManager.s_Instance.GetCurDistrict().GetDPS();
            double dGain = fDPS * item.m_fValue;
            AccountSystem.s_Instance.SetCoin(AccountSystem.s_Instance.GetCoin() + dGain);
            Main.s_Instance.CollectOfflineProfitAnimation();
            OnClickButton_CloseBag();
        }


        if (!bExist && !bDoNotCreateItem) // 不存在正在使用的同ID道具
        {

            the_using_item = ShoppinMall.s_Instance.NewCounter(ShoppinMall.eItemType.coin_raise);
            the_using_item.CopyData(item.m_nItemId);
            the_using_item.transform.SetParent(_containerUsing.transform);
            vecTempScale.x = 1f;
            vecTempScale.y = 1f;
            vecTempScale.z = 1f;
            the_using_item.transform.localScale = vecTempScale;

            the_using_item.Begin();
            m_lstUsingItems_New.Add(the_using_item);


        }
        else
        {

        }

      
        item.SetNum(item.GetNum() - 1); //  个数减1.如果减为0了这个道具会自动销毁

        if (the_using_item)
        {
            switch ((ShoppinMall.eItemType)the_using_item.m_nItemType)
            {
                case ShoppinMall.eItemType.automobile_accelerate:
                    {
                        Main.s_Instance.AccelerateAll(the_using_item.m_fValue, 1);
                        m_fAutomobileSpeedAccelerate = the_using_item.m_fValue;
                    }
                    break;
            } // end switch
        }

        SaveData();

        return the_using_item;

      
       
    }

    float m_fAutomobileSpeedAccelerate = 0;
    public float GetAutomobileSpeedAccelerate()
    {
        return m_fAutomobileSpeedAccelerate;
    }



    List<UIShoppinAndItemCounter> m_lstUsingItems_New = new List<UIShoppinAndItemCounter>();

    List<UIShoppinAndItemCounter> lstTemp = new List<UIShoppinAndItemCounter>();
    public List<UIShoppinAndItemCounter> GetCurUsingItemsList()
    {
        lstTemp.Clear();

        foreach( Transform child in _containerUsing.transform )
        {
            lstTemp.Add( child.gameObject.GetComponent<UIShoppinAndItemCounter>() );
        }

        return lstTemp;
    }

    public void SaveData()
    {
        string szData = "";
        string szKey = "Item";

        // 待使用的道具 right here
        List<UIShoppinAndItemCounter> lstNotUsingItems = _theBag.GetItemsList();
        /*
        foreach( Transform child in _containerNotUsing.transform )
        {
            UIShoppinAndItemCounter item = child.gameObject.GetComponent<UIShoppinAndItemCounter>();
            szData += ( item.m_nItemId + "," + item.GetNum()+ ","  + "0" + "," + "0" + "," +"0" + "_");
        }
        */
        for (int i = 0; i < lstNotUsingItems.Count; i++ )
        {
            UIShoppinAndItemCounter item = lstNotUsingItems[i];
            szData += (item.m_nItemId + "," + item.GetNum() + "," + "0" + "," + "0" + "," + "0" + "_");
        }

        // 正在使用的道具
        foreach (Transform child in _containerUsing.transform)
        {
            UIShoppinAndItemCounter item = child.gameObject.GetComponent<UIShoppinAndItemCounter>();
            szData += (item.m_nItemId + "," + 1 + "," +  "1" + "," + item.GetStartTime() + ","  +item.GetRealDuration() + "_" );
        }

        DataManager.s_Instance.SaveMyData(szKey, szData);

    }

    public void LoadData()
    {
        string szKey = "Item";
        string szData = DataManager.s_Instance.GetMyData_String(szKey);
        string[] aryData = szData.Split( '_' );
        for (int i = 0; i < aryData.Length; i++ )
        {
            if (aryData[i] == "")
            {
                continue;
            }

            string[] aryParams = aryData[i].Split( ',' );



            int nPointer = 0;
            int nItemId = int.Parse(aryParams[nPointer++]);
            int nNum = int.Parse(aryParams[nPointer++]);

            int nStatus = int.Parse(aryParams[nPointer++]);

          

            UIShoppinAndItemCounter item = null;
                         if (nStatus == 1) // 正在使用
            {
                item = ShoppinMall.s_Instance.NewCounter( ShoppinMall.eItemType.coin_raise  );
                item.CopyData(nItemId);
                UIShoppinAndItemCounter the_using_item = ItemSystem.s_Instance.UseItem(item);
                System.DateTime dtStartTime = System.DateTime.Parse(aryParams[nPointer++]);
                int nRealDuration = int.Parse(aryParams[nPointer++]);
                the_using_item.SetStartTime(dtStartTime);
                the_using_item.SetRealDuration(nRealDuration);
            }
            else
            {
                item = ItemSystem.s_Instance.AddToItembag(nItemId, nNum);

            }

        } // end for i

        SaveData();
    }

    public Sprite GetItemIconById( int nItemId )
    {
        Sprite spr = null;

        sItemConfig config = GetItemConfigById(nItemId);

        if (config.nResId >= m_aryItemIcon.Length)
        {
            Debug.LogError("error config.nResId = " + config.nResId);
            return null;
        }


        spr = m_aryItemIcon[config.nResId];
       
        return spr;
    }



} // END CLASS
