﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class ResearchManager : MonoBehaviour
{
    public static ResearchManager s_Instance = null;

    public enum eResearchCounterStatus
    {
        locked,      // 锁定状态
        unlocking,   // 已经预解锁，处在解锁中(倒计时中) 
        unlocked,    // 已解锁
    };

    public struct sResearchConfig
    {
        public double nCoinCost;    // 研究所需金币
        public int nTimeCost;    // 研究所需时长
        public int nDiamondCost; // 钻石购买价格
    };
    sResearchConfig tempResearchConfig;

    Dictionary<string, Dictionary<int, sResearchConfig>> m_dicResearchConfig = new Dictionary<string, Dictionary<int, sResearchConfig>>();

    int m_nWatchAdsReduceTime = 0;
    int m_nWatchAdsInterval = 0;

    public GameObject m_preResearchCounter;
    public GameObject m_preResearchCounterContainer;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start()
    {
        string szConfigFileName_Research = DataManager.url + "research.csv";
        StartCoroutine(LoadConfig_Research(szConfigFileName_Research));
    //    LoadConfigOffline_Research(szConfigFileName_Research);

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void LoadConfigOffline_Research(string szFileName)
    {
        StreamReader sr = new StreamReader(szFileName);
        string szAll = sr.ReadToEnd();         string[] aryLines = szAll.Split('\n');

        string[] aryGeneralParams = aryLines[2].Split(',');

        int nColIndex = 0;
        m_nWatchAdsReduceTime = int.Parse(aryGeneralParams[nColIndex++]);
        m_nWatchAdsInterval = int.Parse(aryGeneralParams[nColIndex++]);

        string[] aryLevels = aryLines[3].Split(',');

        // 每个赛道的配置
        for (int i = 4; i < aryLines.Length; i++)
        {
            string[] aryParams = aryLines[i].Split(',');
            string szKey = aryParams[0];

            if (!MapManager.IsTrackKeyValid(szKey))
            {
                break;
            }

            nColIndex = 1;
            string szLevelkey = aryLevels[nColIndex];
            int nLevelKey = -1;
            Dictionary<int, sResearchConfig> dicParams = new Dictionary<int, sResearchConfig>();
            while (int.TryParse(szLevelkey, out nLevelKey))
            {
                sResearchConfig config = new sResearchConfig();
                string[] arySubParams = aryParams[nColIndex].Split('_');
                config.nCoinCost = double.Parse(arySubParams[0]);
                config.nTimeCost = int.Parse(arySubParams[1]);
                config.nDiamondCost = int.Parse(arySubParams[2]);

                dicParams[nLevelKey] = config;



                nColIndex++;
                if (nColIndex >= aryLevels.Length)
                {
                    break;
                }
                szLevelkey = aryLevels[nColIndex];

            } // end while
            m_dicResearchConfig[szKey] = dicParams;


        } // end for i

    } //end LoadConfigOffline_Research

    IEnumerator LoadConfig_Research(string szFileName)
    {
        WWW www = new WWW(szFileName);
        yield return www; //  待下载


        string[] aryLines = www.text.Split('\n');
        string[] aryGeneralParams = aryLines[2].Split(',');

        int nColIndex = 0;
        m_nWatchAdsReduceTime = int.Parse(aryGeneralParams[nColIndex++]);
        m_nWatchAdsInterval = int.Parse(aryGeneralParams[nColIndex++]);

        string[] aryLevels = aryLines[3].Split(',');
       
        // 每个赛道的配置
        for (int i = 4; i < aryLines.Length; i++)
        {
            string[] aryParams = aryLines[i].Split(',');
            string szKey = aryParams[0];

            if ( !MapManager.IsTrackKeyValid( szKey ) )
            {
                break;
            }

            nColIndex = 1;
            string szLevelkey = aryLevels[nColIndex];
            int nLevelKey = -1;
            Dictionary<int, sResearchConfig> dicParams = new Dictionary<int, sResearchConfig>();
            while ( int.TryParse(szLevelkey, out nLevelKey) )
            {
                sResearchConfig config = new sResearchConfig();
                string[] arySubParams = aryParams[nColIndex].Split( '_' );
                config.nCoinCost = double.Parse(arySubParams[0]);
                config.nTimeCost = int.Parse(arySubParams[1]);
                config.nDiamondCost = int.Parse(arySubParams[2]);

                dicParams[nLevelKey] = config;

   

                nColIndex++;
                if (nColIndex >= aryLevels.Length)
                {
                    break;
                }
                szLevelkey = aryLevels[nColIndex];
     
            } // end while
            m_dicResearchConfig[szKey] = dicParams;

           
        } // end for i
    }

    public sResearchConfig GetResearchConfig( int nPlanetId, int nTrackId, int nLevel, ref bool bFound)
    {
        bFound = false;

        string szKey = nPlanetId + "_" + nTrackId;
        Dictionary<int, sResearchConfig> dicParams = null;
        if ( m_dicResearchConfig.TryGetValue(szKey, out dicParams) )
        {
            if ( dicParams.TryGetValue(nLevel, out tempResearchConfig) )
            {
                bFound = true;
            }
            else
            {
             //  Debug.LogError("error 222");
            }
        }
        else
        {
            //Debug.LogError( "error 111" );
        }

        return tempResearchConfig;
    }

    public Dictionary<int, sResearchConfig> GetResearchConfig( int nPlanetId, int nTrackId )
    {
        string szKey = nPlanetId + "_" + nTrackId;
        return m_dicResearchConfig[szKey];
    }


    public GameObject NewResearchCounterContainer()
    {
        return GameObject.Instantiate( m_preResearchCounterContainer );
    }


    public ResearchCounter NewResearchCounter()
    {
        return GameObject.Instantiate( m_preResearchCounter ).GetComponent<ResearchCounter>();
    }

    public void DeleteResearchCounter( ResearchCounter counter )
    {
        counter.transform.SetParent( ResourceManager.s_Instance.m_goRecycedResearchCounter.transform );
        GameObject.Destroy( counter.gameObject );
    }

    public int GetAdsReduceTime()
    {
        return m_nWatchAdsReduceTime;
    }

    public int GetAdsInterval()
    {
        return m_nWatchAdsInterval;
    }

  


}/// end class
