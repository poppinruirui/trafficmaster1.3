﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdventureManager : MonoBehaviour {

    public static AdventureManager s_Instance = null;

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();

    /// <summary>
    /// UI
    /// </summary>

    public UIAdventureRewardShow[] m_aryAdventureRewardShow;

    public Text _txtLeftTime;
    public Image _txtProgressbar;

    public GameObject _containerProgressBar;

    public Button _btnCollect;
    public Image _imgCollectRewardBg;

    /// end UI

    bool m_bAdventuring = false;
    int m_nLeftTime = 0;
    int m_nTotalTime = 0;

    UIAdventureCounter m_CurAdventure = null;

    // 探险奖励的类型
    public enum eAdventureProfitType
    {
        coin_raise_item,  // 金币强化道具
        green_cash,       // 绿票
    };

    public GameObject _panelAdventure;
    public GameObject _panelAdventuring;

    int m_nRewardStatus = 0; // 0 - none  1 - 滑入  2 - 停顿  3 - 滑出
    int m_nTotalRewardNum = 2;
    int m_nCurShowedRewardNum = 0;
    public float m_fRewardCounterStartPos = 1080f;
    public float m_fRewardCounterEndPos = -1080f;
    public UIAdventureRewardShow[] m_aryRewardShowCounters;
    UIAdventureRewardShow m_CurShowingRewardCounter = null;
    public float m_fShowRewardWaitingTime = 1f;
    public float m_fShowRewardMovingTime = 0.5f;
    float m_fShowRewardMovingSpeed = 0;
    float m_fShowRewardScalingSpeed = 0;

    bool m_bShowingAdventurePanel = false;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {

        m_fShowRewardMovingSpeed = m_fRewardCounterStartPos / m_fShowRewardMovingTime;
        m_fShowRewardScalingSpeed = 1f / m_fShowRewardMovingTime;


    }

    private void FixedUpdate()
    {
        ShowRewardLoop();
    }

    // Update is called once per frame
    void Update () {
        AdventuringLoop();

    }

    public void OnClick_OpenAdventurePanel()
    {
        _panelAdventure.SetActive(true);
        m_bShowingAdventurePanel = true;

    }

    public void OnClick_CloseAdventurePanel()
    {
        _panelAdventure.SetActive(false);
        m_bShowingAdventurePanel = false;
    }

    public bool IsShowingAdventurePanel()
    {
        return m_bShowingAdventurePanel;
    }

    public void BeginAdventure( UIAdventureCounter adventure )
    {
        m_CurAdventure = adventure;
        m_nLeftTime = adventure.m_nDuration;
        m_nTotalTime = adventure.m_nDuration;

        _panelAdventuring.SetActive( true );

        m_bAdventuring = true;

        _btnCollect.gameObject.SetActive(false);

        _containerProgressBar.SetActive(true);
        _imgCollectRewardBg.gameObject.SetActive(false);

        _txtLeftTime.text = "";
        _txtProgressbar.fillAmount = 0;
    }

    public void OnClick_CloseAdventuring()
    {
        _panelAdventuring.SetActive(false);
    }

    float m_fTimeElapse = 0;
    void AdventuringLoop()
    {
        if ( !m_bAdventuring)
        {
            return;
        }

        m_fTimeElapse += Time.deltaTime;
        if (m_fTimeElapse < 1)
        {
            return;
        }
        m_fTimeElapse = 0;

        m_nLeftTime -= 1;

        _txtLeftTime.text = m_nLeftTime.ToString() + "秒后完成探险";

        _txtProgressbar.fillAmount = 1f - (float)m_nLeftTime / m_nTotalTime;

        if (m_nLeftTime <= 0)
        {
            EndAdventure();
        }

    }

    void EndAdventure()
    {
        _txtLeftTime.text = "探险已完成，请收集奖品。";

        m_bAdventuring = false;

        _btnCollect.gameObject.SetActive( true );
        /*
      _panelAdventuring.SetActive( false );

      // 发奖励
      UIMsgBox.s_Instance.ShowMsg( "探险完成，请查收你的奖励" );

      for (int i = 0; i < m_CurAdventure.m_aryProfitList.Length; i++ )
      {
          UIAdventureProfitCounter profit = m_CurAdventure.m_aryProfitList[i];
          switch( profit.m_eProfitType ) 
          {
              case eAdventureProfitType.coin_raise_item:
                  {

                      UIItemInBag bag_item = ResourceManager.s_Instance.NewUiItem();
                      bag_item.InitBagItemById(profit.m_nItemId);
                      ItemSystem.s_Instance.AddItem(bag_item);

                  }
                  break;

              case eAdventureProfitType.green_cash:
                  {
                      AccountSystem.s_Instance.SetGreenCash(AccountSystem.s_Instance.GetGreenCash() + profit.m_nValue0);
                  }
                  break;
          } // end switch
      } // end for
      */
    }


    void BeginShowReward()
    {
        m_nRewardStatus = 1;

        for (int i = 0; i < m_aryRewardShowCounters.Length; i++ )
        {
            UIAdventureRewardShow counter = m_aryRewardShowCounters[i];
            vecTempPos = counter.transform.localPosition;
            vecTempPos.x = m_fRewardCounterStartPos;
            counter.transform.localPosition = vecTempPos;
        }

        m_CurShowingRewardCounter = m_aryRewardShowCounters[0];
        vecTempScale.x = 0f;
        vecTempScale.y = 0f;
        vecTempScale.z = 0f;
        m_CurShowingRewardCounter.transform.localScale = vecTempScale;

        _containerProgressBar.SetActive( false );
        _imgCollectRewardBg.gameObject.SetActive( true );
        _btnCollect.gameObject.SetActive( false );

        m_nCurShowedRewardNum = 0;

        UIAdventureProfitCounter profit = m_CurAdventure.m_aryProfitList[0];
        m_CurShowingRewardCounter.UpdateInfo(profit);

        AudioManager.s_Instance.PlaySE(AudioManager.eSE.e_congratulations);
    }

    float m_fShowRewardTimeElapse = 0;

    void EndShowReward()
    {
        m_nRewardStatus = 0;
        _imgCollectRewardBg.gameObject.SetActive( false );
        _panelAdventuring.SetActive( false );
    }

    void ShowRewardLoop()
    {
        if (m_nRewardStatus == 0)
        {
            return;
        }

        if (m_nRewardStatus == 1) // 滑出
        {
            vecTempPos = m_CurShowingRewardCounter.transform.localPosition;
            vecTempPos.x -= m_fShowRewardMovingSpeed * Time.fixedDeltaTime;

            if (vecTempPos.x <= 0f)
            {
                vecTempPos.x = 0f;
                m_nRewardStatus = 2;
                m_fShowRewardTimeElapse = 0f;
            }

            vecTempScale = m_CurShowingRewardCounter.transform.localScale;
            float fScale = vecTempScale.x;
            fScale += m_fShowRewardScalingSpeed * Time.fixedDeltaTime;
            if (fScale > 1)
            {
                fScale = 1;
            }
            vecTempScale.x = fScale;
            vecTempScale.y = fScale;
            vecTempScale.z = 1f;
            m_CurShowingRewardCounter.transform.localScale = vecTempScale;

            m_CurShowingRewardCounter.transform.localPosition = vecTempPos;
        }
        else if (m_nRewardStatus == 2)
        {
            m_fShowRewardTimeElapse += Time.fixedDeltaTime;
            if (m_fShowRewardTimeElapse >= m_fShowRewardWaitingTime)
            {
                m_nRewardStatus = 3;
            }
        }
        else if (m_nRewardStatus == 3)
        {
            vecTempPos = m_CurShowingRewardCounter.transform.localPosition;
            vecTempPos.x -= m_fShowRewardMovingSpeed * Time.fixedDeltaTime;

            if (vecTempPos.x <= m_fRewardCounterEndPos)
            {
                /*
                vecTempPos.x = 0f;
                m_nRewardStatus = 2;
                m_fShowRewardTimeElapse = 0f;
                */
                m_nCurShowedRewardNum++;
                if (m_nCurShowedRewardNum >= m_nTotalRewardNum)
                {
                    EndShowReward();
                }
                else
                {
                    m_CurShowingRewardCounter = m_aryRewardShowCounters[m_nCurShowedRewardNum];
                    vecTempScale.x = 0f;
                    vecTempScale.y = 0f;
                    vecTempScale.z = 0f;
                    m_CurShowingRewardCounter.transform.localScale = vecTempScale;
                    m_nRewardStatus = 1;

                    UIAdventureProfitCounter profit = m_CurAdventure.m_aryProfitList[m_nCurShowedRewardNum];
                    m_CurShowingRewardCounter.UpdateInfo(profit);

                    return;
                }
            }

            vecTempScale = m_CurShowingRewardCounter.transform.localScale;
            float fScale = vecTempScale.x;
            fScale -= m_fShowRewardScalingSpeed * Time.fixedDeltaTime;

            vecTempScale.x = fScale;
            vecTempScale.y = fScale;
            vecTempScale.z = 1f;
            m_CurShowingRewardCounter.transform.localScale = vecTempScale;

            m_CurShowingRewardCounter.transform.localPosition = vecTempPos;
        }



    } // end ShowRewardLoop()


    public void OnClick_Collect()
    {
       // _panelAdventuring.SetActive(false);

        BeginShowReward();
     


        for (int i = 0; i < m_aryAdventureRewardShow.Length; i++ )
        {
            m_aryAdventureRewardShow[i].gameObject.SetActive( false );
        }

        for (int i = 0; i < m_CurAdventure.m_aryProfitList.Length; i++)
        {
            UIAdventureRewardShow reward_show = m_aryAdventureRewardShow[i];
           // reward_show.gameObject.SetActive( true );


            UIAdventureProfitCounter profit = m_CurAdventure.m_aryProfitList[i];

            reward_show._txtNum.text = profit.m_nValue0.ToString();

            switch (profit.m_eProfitType)
            {
                case eAdventureProfitType.coin_raise_item:
                    {

                        UIItemInBag bag_item = ResourceManager.s_Instance.NewUiItem();
                        bag_item.InitBagItemById(profit.m_nItemId);
                        ItemSystem.s_Instance.AddItem(bag_item);

                        reward_show._txtName.text = profit.m_szName;
                    }
                    break;

                case eAdventureProfitType.green_cash:
                    {
                        AccountSystem.s_Instance.SetGreenCash(AccountSystem.s_Instance.GetGreenCash() + profit.m_nValue0);
                    }
                    break;
            } // end switch
        } // end for
    }




} //  end class
