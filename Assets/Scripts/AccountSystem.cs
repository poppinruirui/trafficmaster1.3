﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AccountSystem : MonoBehaviour {

    public static AccountSystem s_Instance = null;


    public GameObject _uiGeneralMoneyCounters;

    public MoneyCounter[] m_aryGreenCash;

    double m_nGreenCash = 0; // 绿票。 绿票是绑定账号的，金币是绑定某个星球的

    Dictionary<int, int> m_dicTalentPointBuyTimes = new Dictionary<int, int>();

    /// <summary>
    /// 月卡是跟账号绑定的，而不是跟大陆或赛道绑定的
    /// </summary>
    public const int MAX_MONTH_CARDS_NUM = 2;
    MonthCard[] m_aryMonthCards = new MonthCard[MAX_MONTH_CARDS_NUM];


    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {

     
    }

    public void SetGeneralMoneyCountersVisible( bool bVisible  )
    {
        _uiGeneralMoneyCounters.SetActive(bVisible);
    }

    public void InitMoney()
    {
        double dDiamond = DataManager.s_Instance.GetMyData("Diamond");
        if (dDiamond == DataManager.LOAD_MY_DATA_INVALID_VALUE)
        {
            dDiamond = 0d;
        }
        SetGreenCash(dDiamond);

        /*
        for (int i = 0; i < DataManager.s_Instance.MAX_PLANET_NUM; i++)
        {
            double dCoin = DataManager.s_Instance.GetMyData("Coin" + i);
            if (dCoin == DataManager.LOAD_MY_DATA_INVALID_VALUE)
            {
                dCoin = 1000000000000000000000000d; //  每个星球的初始金币值应该是需要配置的，不要写死
            }
            SetCoin(i, dCoin);
        }
        */
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetGreenCash(double nValue)
    {
        m_nGreenCash = nValue;

        //Main.s_Instance._moneyGreenCash.SetValue ( nValue );
        Main.s_Instance.SetDiamond(nValue);

        for (int i = 0; i < m_aryGreenCash.Length; i++ )
        {
            MoneyCounter counter = m_aryGreenCash[i];
            if ( counter == null )
            {
                continue;
            }
            counter.SetValue( nValue );    
        }

        MapManager.s_Instance.SetAllDiamondValueText(nValue);

        DataManager.s_Instance.SaveMyData("Diamond", m_nGreenCash);
    }


    public double GetGreenCash()
    {
        return m_nGreenCash;
    }

    public double GetCoin( int nPlanetId  )
    {
        Planet planet = MapManager.s_Instance.GetPlanetById(nPlanetId);
        //  planet.SetCoin(nValue)
        return planet.GetCoin();
    }

    public void SetCoin(int nPlanetId, double nValue)
    {
        Planet planet = MapManager.s_Instance.GetPlanetById(nPlanetId);
        planet.SetCoin(nValue);


    }

    public double GetCoin()
    {
        return MapManager.s_Instance.GetCurPlanet().GetCoin();
    }

    public void SetCoin( double nValue )
    {
        MapManager.s_Instance.GetCurPlanet().SetCoin( nValue );

    }

    public void ChangeCoin( int nOp, double nValue )
    {
        double nCurCoin = GetCoin();
        if ( nOp > 0 )
        {
            nCurCoin += nValue;
        }
        else if (nOp < 0 )
        {
            nCurCoin -= nValue;
        }

        SetCoin(nCurCoin);

    }


    public void SetTalentPointBuyTime( int nType, int nBuyTimes  )
    {
        m_dicTalentPointBuyTimes[nType] = nBuyTimes;
    }

    public int GetTalentPointBuyTime(int nType)
    {
        int nBuyTimes = 0;
        if ( !m_dicTalentPointBuyTimes.TryGetValue( nType, out nBuyTimes) )
        {
            m_dicTalentPointBuyTimes[nType] = 0;
        }
        return nBuyTimes;
    }

    public int GetCurUsingMonthCardNum()
    {
        int nNum = 0;

        for (int i = 0; i < m_aryMonthCards.Length; i++ )
        {
            if (m_aryMonthCards[i] != null)
            {
                nNum++;
            }
        }

        return nNum;
    }


} // end class
