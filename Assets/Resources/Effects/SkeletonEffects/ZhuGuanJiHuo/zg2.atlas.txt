
zg2.png
size: 458,456
format: RGBA8888
filter: Linear,Linear
repeat: none
fire01
  rotate: true
  xy: 137, 73
  size: 237, 124
  orig: 237, 124
  offset: 0, 0
  index: -1
fire02
  rotate: true
  xy: 2, 80
  size: 230, 133
  orig: 230, 133
  offset: 0, 0
  index: -1
fire03
  rotate: false
  xy: 2, 312
  size: 229, 142
  orig: 229, 142
  offset: 0, 0
  index: -1
fire04
  rotate: true
  xy: 263, 106
  size: 227, 132
  orig: 227, 132
  offset: 0, 0
  index: -1
fire05
  rotate: false
  xy: 233, 335
  size: 223, 119
  orig: 223, 119
  offset: 0, 0
  index: -1
