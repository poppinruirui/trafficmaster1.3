
bj.png
size: 882,882
format: RGBA8888
filter: Linear,Linear
repeat: none
balllight01
  rotate: false
  xy: 604, 482
  size: 148, 148
  orig: 148, 148
  offset: 0, 0
  index: -1
gx1
  rotate: false
  xy: 2, 292
  size: 600, 588
  orig: 600, 588
  offset: 0, 0
  index: -1
light02
  rotate: false
  xy: 293, 106
  size: 185, 184
  orig: 185, 184
  offset: 0, 0
  index: -1
particle02
  rotate: false
  xy: 480, 167
  size: 114, 123
  orig: 114, 123
  offset: 0, 0
  index: -1
ring02
  rotate: false
  xy: 604, 632
  size: 249, 248
  orig: 249, 248
  offset: 0, 0
  index: -1
star
  rotate: false
  xy: 2, 2
  size: 289, 288
  orig: 289, 288
  offset: 0, 0
  index: -1
starparticle
  rotate: false
  xy: 293, 42
  size: 62, 62
  orig: 62, 62
  offset: 0, 0
  index: -1
starparticle-tail
  rotate: false
  xy: 855, 690
  size: 19, 190
  orig: 19, 190
  offset: 0, 0
  index: -1
